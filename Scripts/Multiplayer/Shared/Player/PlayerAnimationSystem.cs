using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

//[DisableAutoCreation]
[UpdateBefore(typeof(TransformSystemGroup))]
public partial class PlayerAnimationSystem : SystemBase
{
    EndSimulationEntityCommandBufferSystem barrier;

    protected override void OnCreate()
    {
        barrier = World.GetOrCreateSystemManaged<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        //var playerNoVRHeadData = GetComponentLookup<PlayerNoVRHead>(true);
        //var playerVRHeadData = GetComponentLookup<PlayerVRHead>(true);
        //var playerVRHandsData = GetComponentLookup<PlayerVRHands>(true);

        //var rotData = GetComponentLookup<Rotation>(true);
        //var posData = GetComponentLookup<Translation>(true);

        var ecb = barrier.CreateCommandBuffer().AsParallelWriter();

        Entities
            //.WithReadOnly(playerNoVRHeadData)
            //.WithReadOnly(playerVRHeadData)
            //.WithReadOnly(playerVRHandsData)
            //.WithReadOnly(rotData)
            //.WithReadOnly(posData)
            .WithAny<PlayerNoVRHead, PlayerVRHead, PlayerVRHands>()
            .ForEach((Entity entity, int entityInQueryIndex, in PlayerAnimation anim) =>
        {
            if (SystemAPI.HasComponent<PlayerNoVRHead>(entity))
            {
                var playerNoVRHeadData = SystemAPI.GetComponent<PlayerNoVRHead>(entity);

                var headTrans = SystemAPI.GetComponent<LocalTransform>(anim.head);
                headTrans.Rotation = quaternion.Euler(new float3(playerNoVRHeadData.headRot * 0.75f, 0));
                ecb.SetComponent(entityInQueryIndex, anim.head, headTrans);

                var neckTrans = SystemAPI.GetComponent<LocalTransform>(anim.neck);
                neckTrans.Rotation = quaternion.Euler(new float3(playerNoVRHeadData.headRot * 0.25f, 0));
                ecb.SetComponent(entityInQueryIndex, anim.neck, neckTrans);
            }
        })
            .ScheduleParallel();

        barrier.AddJobHandleForProducer(Dependency);
    }
}
