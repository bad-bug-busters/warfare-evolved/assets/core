using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;


public struct BindSkeletonRoot : IComponentData
{
    public Entity Value;
}

public struct BoneIndex : IComponentData
{
    public int Value;
}

public struct BoneBindPose : IComponentData
{
    public float4x4 Value;
}

public struct BoneReference : IBufferElementData
{
    public Entity Value;
}

//[UpdateAfter(typeof(Unity.Rendering.EntitiesGraphicsSystem))]
//public partial class ExtraSkinnedMeshRendererConversion : GameObjectConversionSystem
//{
//    protected override void OnUpdate()
//    {
//        Entities.ForEach((SkinnedMeshRenderer meshRenderer) =>
//        {
//            var bindPoses = meshRenderer.sharedMesh.bindposes;
//            if (bindPoses.Length != meshRenderer.bones.Length)
//            {
//                Debug.Log("no matching bone count");
//                return;
//            }
//            var meshEntity = GetPrimaryEntity(meshRenderer);
//            var rootEntity = GetPrimaryEntity(meshRenderer.rootBone);

//            DstEntityManager.AddComponentData(meshEntity, new BindSkeletonRoot { Value = rootEntity });

//            for (int i = 0; i < meshRenderer.bones.Length; i++)
//            {
//                var boneEntity = GetPrimaryEntity(meshRenderer.bones[i]);
//                DstEntityManager.AddComponentData(boneEntity, new BoneBindPose { Value = bindPoses[i] });
//                DstEntityManager.AddComponentData(boneEntity, new BoneIndex { Value = i });
//            }

//            if (!DstEntityManager.HasBuffer<BoneReference>(rootEntity))
//            {
//                var bones = DstEntityManager.AddBuffer<BoneReference>(rootEntity);
//                bones.ResizeUninitialized(meshRenderer.bones.Length);
//                for (int i = 0; i < bones.Length; i++)
//                {
//                    bones[i] = new BoneReference { Value = GetPrimaryEntity(meshRenderer.bones[i]) };
//                }
//            }
//        }).WithStructuralChanges().WithoutBurst().Run();
//    }
//}
