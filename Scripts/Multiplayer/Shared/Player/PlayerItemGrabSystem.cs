//using Unity.Collections;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Physics;
//using Unity.Physics.Systems;
//using Unity.NetCode;
//using Unity.Mathematics;
//using Unity.Transforms;
//using UnityEngine;

//[DisableAutoCreation]
//[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
////[UpdateAfter(typeof(PlayerMoveSystem))]
//public partial class PlayerItemGrabSystem : SystemBase
//{
//    BuildPhysicsWorld buildPhysicsWorld;
//    CollisionFilter collisionFilter;
//    SphereGeometry sphereGeometry;

//    protected override void OnCreate()
//    {
//        buildPhysicsWorld = World.GetOrCreateSystemManaged<BuildPhysicsWorld>();

//        collisionFilter = new CollisionFilter()
//        {
//            BelongsTo = ~0u,
//            CollidesWith = 1u << 4,
//            GroupIndex = 0
//        };

//        sphereGeometry = new SphereGeometry()
//        {
//            Center = float3.zero,
//            Radius = 0.125f
//        };
//    }

//    protected override void OnUpdate()
//    {
//        var ecb = new EntityCommandBuffer(Allocator.Temp);
//        var world = buildPhysicsWorld.PhysicsWorld.CollisionWorld;
//        var filter = collisionFilter;
//        var geometry = sphereGeometry;

//        Entities.ForEach((Entity playerEnt, ref WieldItem wieldItem, in Rotation rot, in Translation pos, in GhostOwnerComponent owner) =>
//        {
//            int inputMode, loopStart, loopEnd;
//            bool2 grip, gripLast;
//            float castLength;
//            var castPos = float3x2.zero;
//            var castRot = float4x2.zero;
//            if (EntityManager.HasBuffer<PlayerVRHands>(playerEnt))
//            {
//                var VRHands = EntityManager.GetComponentData<PlayerVRHands>(playerEnt);
//                var offset = EntityManager.GetComponentData<PlayerVRHead>(playerEnt).pos;
//                offset.y = 0;

//                inputMode = 2;
//                loopStart = 0;
//                loopEnd = 2;
//                grip = VRHands.grip;
//                gripLast = VRHands.gripLast;
//                castLength = 0;

//                castPos = VRHands.pos;
//                for (int i = 0; i < 2; i++)
//                {
//                    castPos[i] += math.rotate(VRHands.rot[i], SettingsVR.handCenter);
//                    castPos[i] -= offset;
//                }
//                castRot = VRHands.rot;

//                VRHands.gripLast = VRHands.grip;
//                ecb.SetComponent(playerEnt, VRHands);
//            }
//            else
//            {
//                var NoVRHands = EntityManager.GetComponentData<PlayerNoVRHands>(playerEnt);
//                loopStart = 1;
//                loopEnd = loopStart + 1;
//                grip = NoVRHands.interact;
//                gripLast = NoVRHands.interactLast;
//                castLength = 2;

//                if (EntityManager.HasBuffer<PlayerVRHead>(playerEnt))
//                {
//                    inputMode = 1;
//                    var VRHead = EntityManager.GetComponentData<PlayerVRHead>(playerEnt);
//                    castPos[loopStart] = VRHead.pos;
//                    castRot[loopStart] = VRHead.rot.value;
//                }
//                else
//                {
//                    inputMode = 0;
//                    var noVRHead = EntityManager.GetComponentData<PlayerNoVRHead>(playerEnt);
//                    castPos[loopStart] = new float3(0, noVRHead.headHeight, 0);
//                    castRot[loopStart] = quaternion.Euler(new float3(noVRHead.headRot, 0)).value;
//                }

//                NoVRHands.interactLast = NoVRHands.interact;
//                ecb.SetComponent(playerEnt, NoVRHands);
//            }

//            for (int cur = loopStart; cur < loopEnd; cur++)
//            {
//                if ((grip[cur] && !gripLast[cur]) || (!grip[cur] && gripLast[cur]))
//                {
//                    var curPos = pos.Value + math.rotate(rot.Value, castPos[cur]);
//                    var curRot = math.mul(rot.Value, castRot[cur]);

//                    //grabbing
//                    if (grip[cur] && !gripLast[cur])
//                    {
//                        var start = curPos;
//                        var end = castLength == 0 ? curPos : curPos + math.rotate(curRot, new float3(0, 0, castLength));
//                        var input = CastInput.Sphere(start, end, filter, geometry);

//                        ColliderCastHit hit;
//                        if (world.CastCollider(input, out hit))
//                        {
//                            var curItem = world.Bodies[hit.RigidBodyIndex].Entity;
//                            //Debug.Log(curItem + " startPos:" + start + " endPos:" + end + " hitPos:" + hit.Position + " dist:" + math.distance(start, hit.Position));

//                            //if (EntityManager.HasBuffer<ItemGrabTag>(curItem))
//                            //{
//                                ecb.SetComponent(curItem, owner);
//                                if (cur == 0)
//                                    wieldItem.leftEntity = curItem;
//                                else
//                                    wieldItem.rightEntity = curItem;

//                                if (EntityManager.HasBuffer<ItemHandOrientation>(curItem))
//                                {
//                                    var hand = EntityManager.GetComponentData<ItemHandOrientation>(curItem);
//                                    if (inputMode == 2)
//                                    {
//                                        var curItemPos = EntityManager.GetComponentData<Translation>(curItem).Value;
//                                        var curItemRot = EntityManager.GetComponentData<Rotation>(curItem).Value;

//                                        hand.offset[cur] = math.rotate(math.inverse(curItemRot), curPos - curItemPos).z;
//                                        var flip = math.dot(math.rotate(curRot, new float3(0, 1, 0)), math.rotate(curItemRot, new float3(0, 1, 0))) < 0 ? true : false;
//                                        var invert = math.dot(math.rotate(curRot, new float3(0, 0, 1)), math.rotate(curItemRot, new float3(0, 0, 1))) < 0 ? true : false;
//                                        if (cur == 0)
//                                        {
//                                            hand.leftFlip = flip;
//                                            hand.leftInvert = invert;
//                                        }
//                                        else
//                                        {
//                                            hand.rightFlip = flip;
//                                            hand.rightInvert = invert;
//                                        }
//                                    }
//                                    else
//                                    {
//                                        hand = new ItemHandOrientation { };
//                                    }
//                                    ecb.SetComponent(curItem, hand);
//                                    //Debug.Log("offset:" + hand.offset[i] + " flip:" + flip + " invert:" + invert);
//                                }
//                                //no hold variation and only one handed
//                                else
//                                {
//                                    //already in other hand? Then switch and reset
//                                    if (wieldItem.leftEntity == wieldItem.rightEntity)
//                                    {
//                                        if (cur == 0)
//                                        {
//                                            wieldItem.rightEntity = Entity.Null;
//                                            ecb.SetComponent(wieldItem.rightEntity, new GhostOwnerComponent { });
//                                        }
//                                        else
//                                        {
//                                            wieldItem.leftEntity = Entity.Null;
//                                            ecb.SetComponent(wieldItem.leftEntity, new GhostOwnerComponent { });
//                                        }
//                                    }

//                                    if (EntityManager.HasBuffer<Arrow>(curItem))
//                                    {
//                                        var arrowData = EntityManager.GetComponentData<Arrow>(curItem);
//                                        if (arrowData.bow != Entity.Null)
//                                        {
//                                            var curItemPos = EntityManager.GetComponentData<Translation>(curItem).Value;
//                                            var curItemRot = EntityManager.GetComponentData<Rotation>(curItem).Value;
//                                            //grab at string
//                                            if (math.rotate(math.inverse(curItemRot), curPos - curItemPos).y > 0)
//                                            {
//                                                arrowData.atString = true;
//                                            }
//                                            //take from string
//                                            else
//                                            {
//                                                var bowData = EntityManager.GetComponentData<Bow>(arrowData.bow);
//                                                bowData.arrow = Entity.Null;
//                                                ecb.SetComponent(arrowData.bow, bowData);

//                                                arrowData.bow = Entity.Null;
//                                                arrowData.atString = false;
//                                            }
//                                        }
//                                        else
//                                        {
//                                            arrowData.atString = false;
//                                        }
//                                        ecb.SetComponent(curItem, arrowData);
//                                    }
//                                }
//                            }

//                            ////ToDo
//                            //else if (EntityManager.HasBuffer<ItemWieldTag>(curItem))
//                            //{
                                
//                            //}
//                        //}
//                    }
//                    //releasing
//                    else if (!grip[cur] && gripLast[cur])
//                    {
//                        Entity curItem;
//                        if (cur == 0)
//                            curItem = wieldItem.leftEntity;
//                        else
//                            curItem = wieldItem.rightEntity;

//                        if (curItem != Entity.Null)
//                        {
//                            ecb.SetComponent(curItem, new GhostOwnerComponent { });
//                            if (cur == 0)
//                                wieldItem.leftEntity = Entity.Null;
//                            else
//                                wieldItem.rightEntity = Entity.Null;

//                            if (EntityManager.HasBuffer<Bow>(curItem))
//                            {
//                                var bowData = EntityManager.GetComponentData<Bow>(curItem);
//                                if (bowData.arrow != Entity.Null)
//                                {
//                                    var arrowData = EntityManager.GetComponentData<Arrow>(bowData.arrow);
//                                    arrowData.bow = Entity.Null;
//                                    ecb.SetComponent(bowData.arrow, arrowData);

//                                    bowData.curDraw = bowData.minDraw;
//                                    bowData.arrow = Entity.Null;
//                                    ecb.SetComponent(curItem, bowData);
//                                }
//                            }
//                            else if (EntityManager.HasBuffer<Arrow>(curItem))
//                            {
//                                var arrowData = EntityManager.GetComponentData<Arrow>(curItem);
//                                if (arrowData.bow == Entity.Null)
//                                {
//                                    var oth = cur == 0 ? 1 : 0;
//                                    var othItem = cur == 0 ? wieldItem.rightEntity : wieldItem.leftEntity;
//                                    if (othItem != Entity.Null && EntityManager.HasBuffer<Bow>(othItem))
//                                    {
//                                        var arrowPos = EntityManager.GetComponentData<Translation>(curItem).Value;
//                                        var bowPos = EntityManager.GetComponentData<Translation>(othItem).Value;
//                                        var bowRot = EntityManager.GetComponentData<Rotation>(othItem).Value;
//                                        var bowData = EntityManager.GetComponentData<Bow>(othItem);
//                                        var stringPos = bowPos + math.rotate(bowRot, new float3(0, bowData.minDraw, 0));

//                                        if (math.distance(arrowPos, stringPos) < 0.125f)
//                                        {
//                                            arrowData.bow = othItem;
//                                            ecb.SetComponent(curItem, arrowData);
//                                            bowData.arrow = curItem;
//                                            ecb.SetComponent(othItem, bowData);
//                                            Debug.Log("arrow:" + curItem + " linked to bow: " + othItem);
//                                        }
//                                    }
//                                }
//                                else
//                                {
//                                    Debug.Log("arrow:" + curItem + " unlinked from bow: " + arrowData.bow);
//                                    var bowData = EntityManager.GetComponentData<Bow>(arrowData.bow);
//                                    //release string
//                                    if (bowData.curDraw != bowData.minDraw)
//                                    {
//                                        var distMax = bowData.maxDraw - bowData.minDraw;
//                                        var distCur = bowData.curDraw - bowData.minDraw;
//                                        var energy = bowData.drawWeight * 9.81 * distCur / distMax;
//                                        var arrowMass = MathNew.RealMass(EntityManager.GetComponentData<PhysicsMass>(curItem).InverseMass);
//                                        var bowMass = MathNew.RealMass(EntityManager.GetComponentData<PhysicsMass>(arrowData.bow).InverseMass);
//                                        var arrowVel = (float)math.sqrt(energy * 0.9f * distCur / (arrowMass + bowMass * 0.04f));

//                                        var arrowRot = EntityManager.GetComponentData<Rotation>(curItem).Value;
//                                        var velData = EntityManager.GetComponentData<PhysicsVelocity>(curItem);
//                                        velData.Linear += math.rotate(arrowRot, new float3(0, -arrowVel, 0));
//                                        ecb.SetComponent(curItem, velData);

//                                        bowData.curDraw = bowData.minDraw;
//                                        bowData.arrow = Entity.Null;
//                                        ecb.SetComponent(arrowData.bow, bowData);
//                                        arrowData.bow = Entity.Null;
//                                        ecb.SetComponent(curItem, arrowData);
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }).WithStructuralChanges().WithoutBurst().Run();

//        ecb.Playback(EntityManager);
//        ecb.Dispose();
//    }
//}