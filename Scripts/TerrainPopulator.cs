using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;
using UnityEditor;

[ExecuteAlways]
public class TerrainPopulator : MonoBehaviour
{
    public bool refresh;
    public bool populate;
    public bool cleanup;

    [Range(1, 256)] public uint seed = 1;

    [Range(0.25f, 1)] public float minScale = 0.75f;
    [Range(1, 2)] public float maxScale = 1.25f;
    public bool randomFlipping = true;
    public bool randomRotation = true;
    [Range(0, 45)] public float randomTilt = 5;
    [Range(0, 1)] public float randomOffset = 1;

    [Range(1, 256)] public float spacing = 64;
    
    public TerrainPrefabList[] prefabLists;

    float halfSpacing;

    float[,] heightData;
    float[,] slopeData;

    TerrainData tData;
    Vector3 terrainSize;
    int layers;

    float[,,] splatMap;

    float2 mapSizeRelation;

    Random random;

    void OnValidate()
    {
        if (populate)
        {
            if (refresh)
            {
                refresh = false;
                Initialize();
            }
            cleanup = true;
            halfSpacing = spacing / 2f;
            //populate = true;
        }
    }

    void Update()
    {
        if (cleanup)
        {
            if (transform.childCount == 0)
            {
                cleanup = false;
            }
            else
            {
                foreach (Transform child in transform)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
        }
        else if (populate)
        {
            Populate();
            populate = false;
        }
    }

    void OnEnable()
    {
        refresh = true;
    }

    void Initialize()
    {
        tData = GetComponent<Terrain>().terrainData;
        terrainSize = tData.size;
        layers = tData.alphamapLayers;
        random = new Random();

        var splatmaps = tData.alphamapTextures;

        heightData = new float[tData.alphamapWidth, tData.alphamapHeight];
        slopeData = new float[tData.alphamapWidth, tData.alphamapHeight];

        mapSizeRelation = new float2(terrainSize.x / tData.alphamapWidth, terrainSize.z / tData.alphamapHeight);

        splatMap = tData.GetAlphamaps(0, 0, tData.alphamapWidth, tData.alphamapHeight);

        for (int x = 0; x < tData.alphamapWidth; x++)
        {
            for (int y = 0; y < tData.alphamapHeight; y++)
            {
                // Get the normalized terrain coordinate that corresponds to the the pointerrain.
                var normX = (float)x / (tData.alphamapWidth - 1);
                var normY = (float)y / (tData.alphamapHeight - 1);
                heightData[x, y] = tData.GetHeight(x, y);
                slopeData[x, y] = tData.GetSteepness(normX, normY);
                //normalData[i] = tData.GetInterpolatedNormal(normY, normX);
            }
        }
    }

    void Populate()
    {
        random.InitState(seed);
        float2 numTrees = new float2(terrainSize.x / spacing, terrainSize.z / spacing);

        for (int x = 0; x < numTrees.x; x++)
        {
            for (int y = 0; y < numTrees.y; y++)
            {
                var treePos = new float2(halfSpacing + x * spacing, halfSpacing + y * spacing);
                
                if (randomOffset != 0)
                {
                    var min = -randomOffset * halfSpacing;
                    var max = randomOffset * halfSpacing;
                    treePos.x += random.NextFloat(min, max);
                    treePos.y += random.NextFloat(min, max);
                }

                var splatPos = treePos / mapSizeRelation;

                if (splatPos.x >= 0 && splatPos.x < tData.alphamapWidth && splatPos.y >= 0 && splatPos.y < tData.alphamapHeight)
                {
                    var maxSplatMap = 0;
                    var maxSplatValue = 0f;
                    for (int i = 0; i < layers; i++)
                    {
                        if (splatMap[(int)splatPos.y, (int)splatPos.x, i] > maxSplatValue)
                        {
                            maxSplatMap = i;
                            maxSplatValue = splatMap[(int)splatPos.y, (int)splatPos.x, i];
                        }
                    }

                    var totalDensity = 0f;
                    var listValue = new float[prefabLists.Length];
                    for (int i = 0; i < listValue.Length; i++)
                    {
                        totalDensity += prefabLists[i].density * prefabLists[i].list.layerDensity[maxSplatMap];
                        listValue[i] = totalDensity;
                    }

                    var value = random.NextFloat(totalDensity);
                    for (int i = 0; i < listValue.Length; i++)
                    {
                        if (value <= listValue[i])//choose prefab list
                        {
                            if (random.NextFloat() <= prefabLists[i].list.layerDensity[maxSplatMap])//density check of current list
                            {
                                totalDensity = 0f;
                                listValue = new float[prefabLists[i].list.prefabs.Length];
                                for (int j = 0; j < listValue.Length; j++)
                                {
                                    totalDensity += prefabLists[i].list.prefabs[j].density;
                                    listValue[j] = totalDensity;
                                }

                                value = random.NextFloat(totalDensity);
                                for (int j = 0; j < listValue.Length; j++)
                                {
                                    if (value <= listValue[j])//choose prefab
                                    {
                                        var instance = PrefabUtility.InstantiatePrefab(prefabLists[i].list.prefabs[j].prefab, transform) as GameObject;

                                        var pos = Vector3.zero;
                                        pos.x = treePos.x;
                                        pos.y = heightData[(int)splatPos.x, (int)splatPos.y];
                                        pos.z = treePos.y;

                                        instance.transform.localPosition = pos;
                                        if (randomRotation)
                                        {
                                            instance.transform.localRotation = Quaternion.Euler(randomTilt == 0 ? 0 : random.NextFloat(-randomTilt, randomTilt), random.NextFloat(360), randomTilt == 0 ? 0 : random.NextFloat(-randomTilt, randomTilt));
                                        }
                                        if (minScale != maxScale)
                                        {
                                            var uniformScale = random.NextFloat(minScale, maxScale);
                                            var scale = new Vector3(uniformScale, uniformScale, uniformScale);
                                            if (randomFlipping)
                                            {
                                                scale.x *= random.NextBool() ? -1 : 1;
                                                scale.z *= random.NextBool() ? -1 : 1;
                                            }

                                            instance.transform.localScale = scale;
                                        }
                                        break;
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    }
}

[System.Serializable]
public struct TerrainPrefabList
{
    public TerrainPrefabListSO list;
    [Range(0, 1)] public float density;
}