﻿using System;
using System.Collections.Generic;
using System.IO;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.XR.Management;

public static class SettingsVR
{
    public static float3 handCenter = new float3(0, 0, -0.1f);
    public static SettingsDataVR curData;

    static string path = Application.persistentDataPath + "/SettingsVR.json";
    static XRManagerSettings xrManager;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
    public static void SubsystemRegistration()
    {
        Debug.Log("system");
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
    public static void AfterAssembliesLoaded()
    {
        Debug.Log("assemblies");
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSplashScreen)]
    public static void BeforeSplashScreen()
    {
        Debug.Log("splash");
        
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void BeforeSceneLoad()
    {
        Debug.Log("before");
        //LoadFile();
        //LoadXR();
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static void AfterSceneLoad()
    {
        Debug.Log("after");
        //SetXR();
    }

    public static void LoadFile()
    {
        if (File.Exists(path))
        {
            curData = JsonUtility.FromJson<SettingsDataVR>(File.ReadAllText(path));
        }
        else
        {
            curData = new SettingsDataVR
            {
                useHandControllers = true,
                bodyHeight = 175
            };
            WriteFile();
        }
    }

    public static void WriteFile()
    {
        File.WriteAllText(path, JsonUtility.ToJson(curData, true));
    }

    public static SettingsDataVR Data
    {
        get
        {
            return curData;
        }
        set
        {
            if (!curData.Equals(value))
            {
                curData = value;
                WriteFile();
            }
        }
    }

    public static void LoadXR()
    {
        xrManager = XRGeneralSettings.Instance.Manager;
        if (curData.enableVR)
            xrManager.InitializeLoader();
    }

    public static void SetXR()
    {
        if (curData.enableVR)
        {
            List<XRInputSubsystem> subsystems = new List<XRInputSubsystem>();
            SubsystemManager.GetInstances(subsystems);
            for (int i = 0; i < subsystems.Count; i++)
            {
                subsystems[i].TrySetTrackingOriginMode(curData.seated ? TrackingOriginModeFlags.Device : TrackingOriginModeFlags.Floor);
            }
        }
        else
        {
            XRGeneralSettings.Instance.Manager.DeinitializeLoader();
        }
    }
}
