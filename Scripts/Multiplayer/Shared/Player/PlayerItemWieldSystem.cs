//using Unity.Collections;
//using Unity.Entities;
//using Unity.Jobs;
//using Unity.Physics;
//using Unity.Physics.Extensions;
//using Unity.Mathematics;
//using Unity.NetCode;
//using Unity.Transforms;

//[DisableAutoCreation]
//[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//[UpdateAfter(typeof(PlayerItemGrabSystem))]
//public partial class PlayerItemWieldSystem : SystemBase
//{
//    protected override void OnUpdate()
//    {
//        var ecb = new EntityCommandBuffer(Allocator.TempJob);
//        var deltaTime = SystemAPI.Time.DeltaTime;

//        Entities.ForEach((Entity playerEnt, ref WieldItem wieldItem, in Rotation rot, in Translation pos, /*in PhysicsVelocity vel,*/ in GhostOwnerComponent owner) =>
//        {
//            var vel = new PhysicsVelocity { };

//            var handPos = float3x2.zero;
//            var handRot = float4x2.zero;

//            if (EntityManager.HasBuffer<PlayerVRHands>(playerEnt))
//            {
//                var VRHands = EntityManager.GetComponentData<PlayerVRHands>(playerEnt);
//                var offset = EntityManager.GetComponentData<PlayerVRHead>(playerEnt).pos;
//                offset.y = 0;

//                handPos = VRHands.pos;
//                handRot = VRHands.rot;
//                for (int i = 0; i < 2; i++)
//                {
//                    handPos[i] -= offset;
//                    handPos[i] += math.rotate(handRot[i], SettingsVR.handCenter);
//                }
//            }
//            else
//            {
//                handPos[0] = new float3(-0.5f, 1, 0);
//                handPos[1] = new float3(0.5f, 1, 0);
//                handRot[0] = quaternion.identity.value;
//                handRot[1] = quaternion.identity.value;
//            }

//            for (int cur = 0; cur < 2; cur++)
//            {
//                var curItem = cur == 0 ? wieldItem.leftEntity : wieldItem.rightEntity;
//                if (curItem != Entity.Null)
//                {
//                    var oth = cur == 0 ? 1 : 0;
//                    var othItem = cur == 0 ? wieldItem.rightEntity : wieldItem.leftEntity;

//                    if (EntityManager.HasBuffer<ItemHandOrientation>(curItem))
//                    {
//                        var hand = EntityManager.GetComponentData<ItemHandOrientation>(curItem);
//                        var curOffRot = quaternion.identity;
//                        var flip = cur == 0 ? hand.leftFlip : hand.rightFlip;
//                        var invert = cur == 0 ? hand.leftInvert : hand.rightInvert;
//                        if (flip)
//                            curOffRot = quaternion.RotateZ(math.PI);
//                        if (invert)
//                            curOffRot = math.mul(curOffRot, quaternion.RotateY(math.PI));

//                        //Debug.Log(flip + " " + invert);

//                        //one handed
//                        if (curItem != othItem)
//                        {
//                            if (EntityManager.HasBuffer<ItemHandOrientation>(curItem))
//                            {
//                                var targetRot = math.mul(handRot[cur], curOffRot);
//                                var targetPos = handPos[cur] - math.rotate(targetRot, new float3(0, 0, hand.offset[cur]));
//                                ApplyVelocity(curItem, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, false, Entity.Null, deltaTime, ref ecb);
//                            }
//                        }
//                        //two handed and first iteration
//                        else
//                        {
//                            var othOffRot = quaternion.identity;
//                            flip = oth == 0 ? hand.leftFlip : hand.rightFlip;
//                            invert = oth == 0 ? hand.leftInvert : hand.rightInvert;
//                            if (flip)
//                                othOffRot = quaternion.RotateZ(math.PI);
//                            if (invert)
//                                curOffRot = math.mul(othOffRot, quaternion.RotateY(math.PI));

//                            var forwardDir = hand.offset[oth] > hand.offset[cur] ? handPos[oth] - handPos[cur] : handPos[cur] - handPos[oth];//check which hand is in front
//                            var targetRot = math.nlerp(math.mul(handRot[cur], curOffRot), math.mul(handRot[oth], othOffRot), 0.5f);
//                            targetRot = quaternion.LookRotationSafe(forwardDir, math.rotate(targetRot, math.up()));
//                            var targetPos = (handPos[cur] + handPos[oth] - math.rotate(targetRot, new float3(0, 0, hand.offset[cur]) + new float3(0, 0, hand.offset[oth]))) / 2;
//                            ApplyVelocity(curItem, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, true, Entity.Null, deltaTime, ref ecb);
//                            break;
//                        }
//                    }

//                    else if (EntityManager.HasBuffer<Bow>(curItem))
//                    {
//                        var curOffPos = new float3(0, 0, -0.04f);
//                        var othOffPos = new float3(0, 0.04f, -0.025f);
//                        if (EntityManager.GetComponentData<Bow>(curItem).arrow != Entity.Null)
//                        {
//                            var bowData = EntityManager.GetComponentData<Bow>(curItem);
//                            var drawDist = bowData.minDraw;
//                            var isDrawing = false;

//                            var targetRot = quaternion.identity;
//                            if (bowData.arrow == othItem)
//                            {
//                                var othPos = handPos[oth] + math.rotate(handRot[oth], new float3(0, 0, -bowData.arrowPosZ * 2));

//                                targetRot = math.nlerp(handRot[cur], handRot[oth], 0.25f);
//                                targetRot = math.mul(targetRot, quaternion.RotateX(math.radians(-90)));
//                                targetRot = quaternion.LookRotationSafe(othPos - handPos[cur], math.rotate(targetRot, math.up()));
//                                targetRot = math.mul(targetRot, quaternion.RotateX(math.radians(90)));

//                                var curOffset = handPos[cur] - math.rotate(targetRot, curOffPos);
//                                var otherOffset = othPos - math.rotate(targetRot, othOffPos);

//                                drawDist = math.clamp(math.rotate(math.inverse(targetRot), otherOffset - curOffset).y, bowData.minDraw, bowData.maxDraw);
//                                isDrawing = true;
//                            }
//                            else
//                            {
//                                targetRot = handRot[cur];
//                            }

//                            var targetPos = handPos[cur] - math.rotate(targetRot, curOffPos);
//                            ApplyVelocity(curItem, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, true, bowData.arrow, deltaTime, ref ecb);

//                            targetPos += math.rotate(targetRot, new float3(0, drawDist, bowData.arrowPosZ));
//                            var arrowRotZ = math.atan((bowData.gribRadius + 0.005f) / drawDist);
//                            targetRot = math.mul(targetRot, quaternion.RotateZ(cur == 0 ? -arrowRotZ : arrowRotZ));

//                            //EntityManager.SetComponentData(bowData.arrow, new Translation { Value = targetPos });
//                            //EntityManager.SetComponentData(bowData.arrow, new Rotation { Value = targetRot });
//                            //EntityManager.SetComponentData(bowData.arrow, new PhysicsVelocity { });

//                            ApplyVelocity(bowData.arrow, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, true, curItem, deltaTime, ref ecb);

//                            if (isDrawing)
//                            {
//                                bowData.curDraw = drawDist;
//                                ecb.SetComponent(curItem, bowData);
//                                break;
//                            }
//                        }
//                        else
//                        {
//                            var targetRot = handRot[cur];
//                            var targetPos = handPos[cur] - math.rotate(targetRot, curOffPos);
//                            ApplyVelocity(curItem, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, false, Entity.Null, deltaTime, ref ecb);
//                        }
//                    }
//                    else if (EntityManager.HasBuffer<Arrow>(curItem))
//                    {
//                        var arrowData = EntityManager.GetComponentData<Arrow>(curItem);
//                        if (arrowData.bow == Entity.Null)
//                        {
//                            var targetRot = handRot[cur];
//                            var targetPos = handPos[cur];
//                            ApplyVelocity(curItem, targetPos, targetRot, pos.Value, rot.Value, vel.Linear, false, Entity.Null, deltaTime, ref ecb);
//                        }
//                    }
//                }
//            }
//        }).WithoutBurst().Run();

//        ecb.Playback(EntityManager);
//        ecb.Dispose();
//    }

//    void ApplyVelocity(Entity item, float3 targetPos, quaternion targetRot, float3 basePos, quaternion baseRot, float3 baseVel, bool twoHanded, Entity linkedItem, float deltaTime, ref EntityCommandBuffer ecb)
//    {
//        var posData = EntityManager.GetComponentData<Translation>(item);
//        var rotData = EntityManager.GetComponentData<Rotation>(item);
//        var massData = EntityManager.GetComponentData<PhysicsMass>(item);

//        var velData = EntityManager.GetComponentData<PhysicsVelocity>(item);

//        targetPos = basePos + math.rotate(baseRot, targetPos);
//        targetRot = math.mul(baseRot, targetRot);

//        var curCOM = linkedItem != Entity.Null ? posData.Value + math.rotate(rotData.Value, massData.CenterOfMass) : posData.Value;
//        var targetCOM = linkedItem != Entity.Null ? targetPos + math.rotate(targetRot, massData.CenterOfMass) : targetPos;
//        var reqVel = math.distance(curCOM, targetCOM) / deltaTime;

//        var mass = MathNew.RealMass(massData.InverseMass);
//        if (linkedItem != Entity.Null)
//            mass += MathNew.RealMass(EntityManager.GetComponentData<PhysicsMass>(linkedItem).InverseMass);

//        var swingEnergy = 150;
//        var maxVel = math.sqrt((twoHanded ? swingEnergy * 2 : swingEnergy) / (1 + mass));
//        maxVel += math.length(baseVel);//add player movement speed
//        var modVel = math.clamp(maxVel / reqVel, 0, 1) / deltaTime;

//        velData.Linear = (targetPos - posData.Value) * modVel;
//        velData.SetAngularVelocityWorldSpace(massData, rotData, MathNew.ToEulerAngles(math.mul(targetRot, math.inverse(rotData.Value))) * modVel);
//        ecb.SetComponent(item, velData);
//    }
//}
