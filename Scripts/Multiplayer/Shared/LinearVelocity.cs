using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


[GhostComponent(SendTypeOptimization = GhostSendType.OnlyPredictedClients, OwnerSendType = SendToOwnerType.SendToOwner, PrefabType = GhostPrefabType.AllPredicted)]
public struct LinearVelocity : IComponentData
{
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float3 Value;
}
