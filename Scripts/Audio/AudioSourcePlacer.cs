using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;
using UnityEditor;

[ExecuteAlways]
public class AudioSourcePlacer : MonoBehaviour
{
    public bool place;
    public bool clear;

    public GameObject[] birds;
    [Range(1, 100)] public int treesPerBird = 10;

    public GameObject[] orthopteras;
    [Range(1, 100)] public int bushesPerOrthoptera = 10;

    Random random;

    void OnValidate()
    {
        random = new Random(1);
        if (place)
        {
            clear = true;
        }
    }

    void Awake()
    {
        place = false;
        clear = false;
    }

    void Update()
    {
        if (clear)
        {
            if (transform.childCount == 0)
            {
                clear = false;
            }
            else
            {
                foreach (Transform child in transform)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
        }
        else if (place)
        {
            place = false;

            for (int i = 0; i < 2; i++)
            {
                var plants = GameObject.FindGameObjectsWithTag(i == 0 ? "Tree" : "Bush");
                foreach (var plant in plants)
                {
                    if (random.NextInt(i == 0 ? treesPerBird : bushesPerOrthoptera) == 0)
                    {
                        var instance = PrefabUtility.InstantiatePrefab(i == 0 ? birds[random.NextInt(birds.Length)] : orthopteras[random.NextInt(orthopteras.Length)], transform) as GameObject;
                        instance.transform.position = plant.GetComponentInChildren<Renderer>().bounds.center;
                    }
                }
            }
        }
    }
}
