﻿using Unity.Entities;
using Unity.NetCode;


public struct Bow : IComponentData
{
    public Entity arrow;
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.Interpolate)] public float arrowPosZ;
    public float drawWeight;
    public float minDraw;
    public float maxDraw;
    public float curDraw;
    public float gribRadius;
}
