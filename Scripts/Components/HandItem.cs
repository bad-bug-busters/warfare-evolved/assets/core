﻿using Unity.Entities;
using Unity.Mathematics;


public struct HandItem : IComponentData
{
    public Entity Item;
    public float3 offsetPos;
    public quaternion offsetRot;
}
