using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.XR;

//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
[UpdateInGroup(typeof(PresentationSystemGroup))]
//[UpdateAfter(typeof(PlayerAnimationSystem))]
public partial class CameraRigSystem : SystemBase
{
    Transform cameraRig;
    //float3 lastPos;

    protected override void OnCreate()
    {
        RequireForUpdate<CameraRigTag>();
        RequireForUpdate<LocalPlayerTag>();
        RequireForUpdate<SettingsDataVR>();
    }
    protected override void OnUpdate()
    {
        var rig = SystemAPI.GetSingletonEntity<CameraRigTag>();
        if (cameraRig == null)
            cameraRig = EntityManager.GetComponentObject<Transform>(rig);

        var player = SystemAPI.GetSingletonEntity<LocalPlayerTag>();
        var trans = EntityManager.GetComponentData<LocalTransform>(player);

        if (SystemAPI.GetSingleton<SettingsDataVR>().enableVR)
        {
            var offset = SystemAPI.GetSingleton<InputVRHead>().pos;
            offset.y = 0;
            trans.Position -= math.rotate(trans.Rotation, offset);

            //if (SettingsVR.Data.useHandControllers)
            //{
            //    var hands = SystemAPI.GetSingleton<InputVRHands>();
            //    for (int i = 0; i < 2; i++)
            //    {
            //        cameraRig.GetChild(i + 1).localPosition = hands.pos[i];
            //        cameraRig.GetChild(i + 1).localRotation = (quaternion)hands.rot[i];
            //    }
            //}
        }
        else
        {
            var noVRHead = EntityManager.GetComponentData<PlayerNoVRHead>(player);
            cameraRig.GetChild(0).localPosition = new Vector3(0, noVRHead.headHeight, 0.1f);
            cameraRig.GetChild(0).localRotation = quaternion.Euler(new float3(noVRHead.headRot, 0));
        }

        EntityManager.SetComponentData(rig, trans);
    }
}
