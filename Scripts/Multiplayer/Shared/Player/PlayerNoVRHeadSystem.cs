using Unity.Entities;
using Unity.NetCode;
using Unity.Mathematics;

//[DisableAutoCreation]
[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
public partial class PlayerNoVRHeadSystem : SystemBase
{
    float rotSpeed = 4f / sbyte.MaxValue;
    float maxVertical = math.radians(75);
    float maxHorizontal = math.radians(90);

    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;
        var deltaTime = SystemAPI.Time.DeltaTime;
        var rotSpeed = this.rotSpeed;
        var maxVertical = this.maxVertical;
        var maxHorizontal = this.maxHorizontal;

        Entities
            .ForEach((DynamicBuffer<InputPlayerNonVR> buffer, ref PlayerNoVRHead noVRHead, in PredictedGhostComponent prediction) =>
        {
            buffer.GetDataAtTick(tick, out InputPlayerNonVR input);
            if (input.noVRHead.rotX != 0)
            {
                noVRHead.headRot[0] -= input.noVRHead.rotX * rotSpeed * deltaTime;
                noVRHead.headRot[0] = math.clamp(noVRHead.headRot[0], -maxVertical, maxVertical);
            }

            if (input.noVRHead.freeLook)
            {
                if (input.general.rotY != 0)
                {
                    noVRHead.headRot[1] += input.general.rotY * rotSpeed * deltaTime;
                    noVRHead.headRot[1] = math.clamp(noVRHead.headRot[1], -maxHorizontal, maxHorizontal);
                }
            }
            else if (noVRHead.headRot[1] != 0)
            {
                if (noVRHead.headRot[1] < 0)
                {
                    noVRHead.headRot[1] += rotSpeed * deltaTime;
                    noVRHead.headRot[1] = math.min(noVRHead.headRot[1], 0);
                }
                else
                {
                    noVRHead.headRot[1] -= rotSpeed * deltaTime;
                    noVRHead.headRot[1] = math.max(noVRHead.headRot[1], 0);
                }
            }
            noVRHead.headHeight = 1.65f;
        })
            .ScheduleParallel();
    }
}
