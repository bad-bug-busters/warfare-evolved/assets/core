﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

[ExecuteAlways]
public class TerrainPainter : MonoBehaviour
{
    public bool refresh;
    public bool paint;
    [Range(0, 1)] public float randomness;
    [Range(0, 1)] public float[] weight = new float[8];
    [Tooltip("Base, Range, Exponent")] public Vector3[] heightFactor = new Vector3[8];
    [Tooltip("Base, Range, Exponent")] public Vector3[] slopeFactor = new Vector3[8];

    TerrainData tData;
    int size;
    int sizeSq;
    int layers;

    NativeArray<float> heightData = new NativeArray<float>();
    NativeArray<float> slopeData = new NativeArray<float>();

    float[,,] splatMap;

    void OnValidate()
    {
        if (paint)
        {
            if (refresh)
            {
                refresh = false;
                Initialize();
            }
            paint = false;
            Paint();
        }
    }

    void Start()
    {
        refresh = true;
        //Debug.Log("start");
    }

    void OnDestroy()
    {
        Cleanup();
        //Debug.Log("destroy");
    }

    void OnEnable()
    {
        refresh = true;
        //Debug.Log("enable");
    }

    void OnDisable()
    {
        Cleanup();
        //Debug.Log("disable");
    }

    void Initialize()
    {
        tData = GetComponent<Terrain>().terrainData;

        size = tData.alphamapWidth;
        sizeSq = size * size;
        layers = tData.alphamapLayers;

        heightData = new NativeArray<float>(sizeSq, Allocator.Persistent);
        slopeData = new NativeArray<float>(sizeSq, Allocator.Persistent);

        for (int i = 0; i < sizeSq; i++)
        {
            var x = i % size;
            var y = i / size;
            // Get the normalized terrain coordinate that corresponds to the the pointerrain.
            var normX = (float)x / (size - 1);
            var normY = (float)y / (size - 1);
            //wrong parameter order LOL, took me ages to find the "bug"
            heightData[i] = tData.GetHeight(y, x) + transform.position.y;
            slopeData[i] = tData.GetSteepness(normY, normX);
            //normalData[i] = tData.GetInterpolatedNormal(normY, normX);
        }
        splatMap = new float[size, size, layers];
    }

    void Paint()
    {
        var startTime = Time.realtimeSinceStartup;

        var layerWeight = new NativeArray<float>(layers, Allocator.TempJob);
        var layerHeight = new NativeArray<float3>(sizeSq, Allocator.TempJob);
        var layerSlope = new NativeArray<float3>(sizeSq, Allocator.TempJob);

        var splatData = new NativeArray<float>(sizeSq * layers, Allocator.TempJob);

        var maxThreads = Unity.Jobs.LowLevel.Unsafe.JobsUtility.MaxJobThreadCount;

        var random = new NativeArray<Random>(maxThreads, Allocator.TempJob);

        for (int i = 0; i < layers; i++)
        {
            layerWeight[i] = weight[i];
            layerHeight[i] = heightFactor[i];
            layerSlope[i] = slopeFactor[i];
        }

        for (int i = 0; i < random.Length; i++)
        {
            random[i] = new Random((uint)(i + 1));
        }

        var job = new PaintJob()
        {
            layers = layers,
            heightData = heightData,
            slopeData = slopeData,
            //normalData = normalData,
            layerWeight = layerWeight,
            layerHeight = layerHeight,
            layerSlope = layerSlope,
            splatData = splatData,
            random = random,
            randomness = randomness
        };
        var handle = job.Schedule(sizeSq, 1);
        handle.Complete();

        for (int i = 0; i < sizeSq; i++)
        {
            var x = i % size;
            var y = i / size;
            for (int j = 0; j < layers; j++)
            {
                splatMap[x, y, j] = splatData[i * layers + j];
            }
        }
        tData.SetAlphamaps(0, 0, splatMap);
        splatData.Dispose();

        Debug.Log("Painting took; " + (Time.realtimeSinceStartup - startTime));
    }

    void Cleanup()
    {
        heightData.Dispose();
        slopeData.Dispose();
    }
}

public struct PaintJob : IJobParallelFor
{
    [Unity.Collections.LowLevel.Unsafe.NativeSetThreadIndex]
    int threadIndex;

    [ReadOnly] public int layers;
    [ReadOnly] public float randomness;
    [ReadOnly] public NativeArray<float> heightData;
    [ReadOnly] public NativeArray<float> slopeData;
    //[ReadOnly, DeallocateOnJobCompletion] public NativeArray<float3> normalData;
    [ReadOnly, DeallocateOnJobCompletion] public NativeArray<float> layerWeight;
    [ReadOnly, DeallocateOnJobCompletion] public NativeArray<float3> layerHeight;
    [ReadOnly, DeallocateOnJobCompletion] public NativeArray<float3> layerSlope;
    [NativeDisableParallelForRestriction] public NativeArray<float> splatData;
    [NativeDisableParallelForRestriction, DeallocateOnJobCompletion] public NativeArray<Random> random;

    public void Execute(int i)
    {
        var rnd = random[threadIndex];
        var iPos = i * layers;
        var totalSplat = 0f;
        for (int j = 0; j < layers; j++)
        {
            if (layerWeight[j] != 0)
            {
                var splat = layerWeight[j];
                if (layerHeight[j].y != 0)
                {
                    splat *= layerHeight[j].y / math.max(math.abs(layerHeight[j].x - heightData[i]), layerHeight[j].y);
                    if (layerHeight[j].z != 1)
                    {
                        splat *= math.pow(splat, layerHeight[j].z);
                    }
                }
                if (layerSlope[j].y != 0)
                {
                    splat *= layerSlope[j].y / math.max(math.abs(layerSlope[j].x - slopeData[i]), layerSlope[j].y);
                    if (layerSlope[j].z != 1)
                    {
                        splat *= math.pow(splat, layerSlope[j].z);
                    }
                }
                if (randomness != 0)
                {
                    splat *= 1 - rnd.NextFloat() * randomness;
                }
                
                splatData[iPos + j] += splat;
                totalSplat += splat;
            }
        }

        if (totalSplat == 0)
        {
            splatData[iPos] = 1;
        }
        else
        {
            for (int j = 0; j < layers; j++)
            {
                splatData[iPos + j] /= totalSplat;
            }
        }
        
        random[threadIndex] = rnd;
    }
}