﻿using Unity.Entities;
using Unity.Mathematics;


public struct Friction : IComponentData
{
    public float crossSection;
    public float dragCoefficient;
    public float3 centerOfPressure;
    public float3 dragSpin;
    public float drag;
}
