﻿using Unity.NetCode;

public struct InputPlayerNonVR : ICommandData
{
    public NetworkTick Tick {get; set;}
    public InputGeneral general;
    public InputNoVRHead noVRHead;
    public InputNoVRHands noVRHands;
}
