using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateBefore(typeof(InputGeneralSystem))]
public partial class InputNoVRHandsSystem : SystemBase
{
    protected override void OnCreate()
    {
        if (!SettingsVR.curData.enableVR || !SettingsVR.curData.useHandControllers)
        {
            EntityManager.CreateEntity(typeof(InputNoVRHands));
        }
        RequireForUpdate<InputNoVRHands>();
    }

    protected override void OnUpdate()
    {
        var input = SystemAPI.GetSingleton<InputNoVRHands>();

        input.primaryAttack = Input.GetButton("Interact");
        input.secondaryAttack = Input.GetButton("Interact");
        input.block = Input.GetButton("Interact");
        input.offhandAttack = Input.GetButton("Interact");
        input.interact = Input.GetButton("Interact");

        SystemAPI.SetSingleton(input);
    }
}
