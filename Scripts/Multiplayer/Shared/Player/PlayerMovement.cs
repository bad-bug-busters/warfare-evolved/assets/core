﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


public struct PlayerMovement : IComponentData
{
    public float moveX;
    public float moveZ;
    public quaternion heading;
    public bool sprint;
    public bool jump;
    public bool jumpLast;
}
