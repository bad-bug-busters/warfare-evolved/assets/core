﻿using Unity.Entities;
using Unity.NetCode;

[WorldSystemFilter(WorldSystemFilterFlags.ServerSimulation | WorldSystemFilterFlags.ClientSimulation)]
[UpdateBefore(typeof(RpcCommandRequestSystemGroup))]
public class RpcReceiveSystemGroup : ComponentSystemGroup
{
}
