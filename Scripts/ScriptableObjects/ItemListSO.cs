﻿using UnityEngine;

[CreateAssetMenu(fileName = "ItemList", menuName = "ScriptableObjects/ItemList", order = 1)]
public class ItemListSO : ScriptableObject
{
    public GameObject[] item;
}
