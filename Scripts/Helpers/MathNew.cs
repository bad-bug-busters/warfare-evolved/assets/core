﻿using Unity.Mathematics;

public static class MathNew
{
    public const float epsilon = 1.192093e-07f;//machine epsilon

    public static float3 ToEulerAngles(quaternion q)
    {
        if (math.abs(q.value.w) > 1f - epsilon)
            return float3.zero;
        var wSign = q.value.w >= 0 ? 1f : -1f;
        var angle = math.acos(wSign * q.value.w);
        var gain = wSign * 2f * angle / math.sin(angle);
        return new float3(q.value.xyz * gain);
    }

    public static float3 ProjectOnPlane(float3 vec, float3 normal)
    {
        return vec - normal * math.dot(vec, normal);
    }

    //public static quaternion FromToRotation(float3 from, float3 to)
    //{
    //    var axis = math.normalize(math.cross(from, to));
    //    var angle = math.acos(math.clamp(math.dot(math.normalize(from), math.normalize(to)), -1f, 1f));
    //    return quaternion.AxisAngle(axis, angle);
    //}

    public static quaternion FromToRotation(float3 from, float3 to)
    {
        float theta = math.dot(math.normalize(from), math.normalize(to));
        if (theta >= 1f)
            return quaternion.identity;

        if (theta <= -1f)
        {
            float3 axis = math.cross(from, math.right());
            if (math.lengthsq(axis) == 0f)
                axis = math.cross(from, math.up());

            return quaternion.AxisAngle(axis, math.radians(180f));
        }

        return quaternion.AxisAngle(math.normalize(math.cross(from, to)), math.acos(theta));
    }

    public static float RealMass(float invMass)
    {
        return math.rcp(invMass);
    }
}
