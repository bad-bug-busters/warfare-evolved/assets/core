using Unity.Mathematics;
using UnityEngine;

[CreateAssetMenu(fileName = "TerrainPrefabList", menuName = "ScriptableObjects/TerrainPrefabList", order = 1)]
public class TerrainPrefabListSO : ScriptableObject
{
    [Range(0, 1)] public float[] layerDensity = new float[8];
    public bool calcPrefabDensity;
    public TerrainPrefab[] prefabs;

    void OnValidate()
    {
        if (calcPrefabDensity)
        {
            calcPrefabDensity = false;
            for (int i = 0; i < prefabs.Length; i++)
            {
                var size = 0f;
                var renderers = prefabs[i].prefab.GetComponentsInChildren<Renderer>();
                foreach (var renderer in renderers)
                {
                    var curSize = renderer.bounds.size.magnitude;
                    if (size < curSize)
                    {
                        size = curSize;
                    }
                }
                prefabs[i].density = 1 / math.sqrt(1 + size);
            }
        }
    }
}

[System.Serializable]
public struct TerrainPrefab
{
    public GameObject prefab;
    [Range(0, 1)] public float density;
}
