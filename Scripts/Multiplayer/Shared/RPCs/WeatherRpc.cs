﻿//using Unity.Burst;
using Unity.Entities;
//using Unity.NetCode;
//using Unity.Networking.Transport;

//[BurstCompile]
public struct WeatherRpc : IComponentData/*, IRpcCommandSerializer<WeatherRpc>*/
{
    public Weather weather;

    //public PortableFunctionPointer<RpcExecutor.ExecuteDelegate> CompileExecute()
    //{
    //    return new PortableFunctionPointer<RpcExecutor.ExecuteDelegate>(InvokeExecute);
    //}

    //public void Serialize(ref DataStreamWriter writer, in WeatherRpc data)
    //{
    //    writer.WriteUShort((ushort)(data.weather.fogDistance - 1));
    //    writer.WriteByte((byte)(data.weather.clouds * byte.MaxValue));
    //    for (int i = 0; i < 2; i++)
    //    {
    //        writer.WriteByte((byte)(data.weather.wind[i] * 10 + 128));
    //    }
    //}

    //public void Deserialize(ref DataStreamReader reader, ref WeatherRpc data)
    //{
    //    data.weather.fogDistance = reader.ReadUShort() + 1;
    //    data.weather.clouds = reader.ReadByte() / (float)byte.MaxValue;
    //    for (int i = 0; i < 2; i++)
    //    {
    //        data.weather.wind[i] = (reader.ReadByte() - 128) / 10f;
    //    }
    //}

    //[BurstCompile]
    //private static void InvokeExecute(ref RpcExecutor.Parameters parameters)
    //{
    //    RpcExecutor.ExecuteCreateRequestComponent<WeatherRpc, WeatherRpc>(ref parameters);
    //}
}

//public class WeatherRpcSystem : RpcCommandRequestSystem<WeatherRpc, WeatherRpc>
//{
//}