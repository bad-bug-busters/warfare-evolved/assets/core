﻿using Unity.Entities;
using UnityEngine;

[System.Serializable]

public struct DayTime : IComponentData
{
    [Range(0, 1)] public float value;
    [Range(0, 255)] public byte scale;
}
