﻿using Unity.Entities;
using Unity.Mathematics;

public struct InputVRHands : IComponentData
{
    public float3x2 pos;
    public float4x2 rot;
    public bool2 trigger;
    public bool2 grip;
}
