﻿//using Unity.Collections;
//using Unity.Entities;
//using Unity.Physics;
//using Unity.Physics.Systems;
//using Unity.Mathematics;
//using Unity.NetCode;
//using Unity.Transforms;
//using RaycastHit = Unity.Physics.RaycastHit;
//using Random = Unity.Mathematics.Random;

////public struct PropCheckSupport : IComponentData
////{
////    public double checkTime;
////}

//[DisableAutoCreation]
//[UpdateInGroup(typeof(ServerSimulationSystemGroup))]
//public class PropCheckSupportSystem : ComponentSystem
//{
//    BuildPhysicsWorld buildPhysicsWorld;
//    CollisionFilter collisionFilter;

//    NativeArray<float3> dir = new NativeArray<float3>(6, Allocator.Persistent);

//    int reqSupp = 1;
//    int interval = 1;

//    Random randStart;
//    protected override void OnCreate()
//    {
//        buildPhysicsWorld = World.GetOrCreateSystemManaged<BuildPhysicsWorld>();

//        collisionFilter = new CollisionFilter()
//        {
//            BelongsTo = ~0u,
//            CollidesWith = 1u << 0,
//            GroupIndex = 0
//        };

//        dir[0] = new float3(0, -0.5f, 0);
//        dir[1] = new float3(-0.5f, 0, 0);
//        dir[2] = new float3(0.5f, 0, 0);
//        dir[3] = new float3(0, 0 ,-0.5f);
//        dir[4] = new float3(0, 0, 0.5f);
//        dir[5] = new float3(0, 0.5f, 0);

//        randStart = new Random(1);
//    }

//    protected override void OnUpdate()
//    {
//        var world = buildPhysicsWorld.PhysicsWorld.CollisionWorld;
//        var filter = collisionFilter;
//        var time = Time.ElapsedTime;
//        var rand = randStart;
//        Entities.ForEach((Entity entity, ref Translation pos, ref Rotation rot, ref PropCheckSupport support, ref SceneProp prop) =>
//        {
//            if (support.checkTime == 0)
//                support.checkTime = time + rand.NextDouble(0, interval);

//            if (support.checkTime > time)
//                return;

//            support.checkTime += interval;

//            var curSupp = 0;
//            for (int i = 0; i < dir.Length; i++)
//            {
//                var input = CastInput.Ray(pos.Value + dir[i], pos.Value, filter);
//                RaycastHit hit;
//                if (world.CastRay(input, out hit))
//                {
//                    var hitEnt = world.Bodies[hit.RigidBodyIndex].Entity;
//                    if (hitEnt != entity)
//                    {
//                        curSupp++;
//                        if (curSupp >= reqSupp)
//                            break;
//                        //UnityEngine.Debug.Log("suppEnt " + hitEnt);
//                    }
                    
//                }
//            }

//            if (curSupp < reqSupp)
//            {
//                var newEvent = EntityManager.CreateEntity();
//                EntityManager.AddComponentData(newEvent, new DestroyPropEvent { id = prop.id, isServer = true });

//                var id = prop.id;
//                //broadcast
//                Entities.WithAll<NetworkStreamInGame>().ForEach((Entity ent, ref NetworkIdComponent netId) =>
//                {
//                    var reqEnt = EntityManager.CreateEntity();
//                    EntityManager.AddComponentData(reqEnt, new DestroyPropRpc { id = id });
//                    EntityManager.AddComponentData(reqEnt, new SendRpcCommandRequestComponent { TargetConnection = ent });
//                });
//            }
//        });
//    }
//}
