using Unity.Mathematics;
using UnityEngine;

public class MaterialBendFactor : MonoBehaviour
{
    void OnValidate()
    {
        var height = 0f;
        var mpb = new MaterialPropertyBlock();
        var renderers = GetComponentsInChildren<Renderer>();

        for (int i = 0; i < 2; i++)
        {
            if (i == 1)
            {
                mpb.SetFloat("_BendFactor", 0.25f / math.pow(1f + height, 0.75f) * transform.localScale.y);
            }
            for (int j = 0; j < renderers.Length; j++)
            {
                if (i == 1)
                {
                    renderers[j].SetPropertyBlock(mpb);
                }
                else if (height < renderers[j].bounds.size.y)
                {
                    height = renderers[j].bounds.size.y;
                }
            }
        }
    }
}
