﻿using Unity.Entities;
using Unity.Mathematics;


public struct AgentAnimation : IComponentData
{
    public float3 bodyPos;
    public quaternion bodyRot;
}
