using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public struct TwoBoneInput
{
    public float3 rootPos;
    public quaternion rootRot;

    public float3 midPos;
    public quaternion midRot;

    public float3 tipPos;
    public quaternion tipRot;

    public float3 targetPos;
    public quaternion targetRot;

    public float3 hintPos;

    public float3 offsetPos;
    public quaternion offsetRot;

    public float rootLength;
    public float tipLength;

    public bool hasHint;

    public float posWeight;
    public float rotWeight;
    public float hintWeight;
}

public struct TwoBoneOutput
{
    public quaternion rootRot;
    public quaternion midRot;
    public quaternion tipRot;
}

public static class IKSolver
{
    const float k_SqrEpsilon = 1e-8f;

    public static TwoBoneOutput TwoBone(TwoBoneInput input)
    {
        var tPosition = math.lerp(input.tipPos, input.targetPos + input.offsetPos, input.posWeight);
        var tRotation = math.slerp(input.tipRot, math.mul(input.targetRot, input.offsetRot), input.rotWeight);

        var ab = input.midPos - input.rootPos;
        var bc = input.tipPos - input.midPos;
        var ac = input.tipPos - input.rootPos;
        var at = tPosition - input.rootPos;

        var acLen = math.length(ac);
        var atLen = math.length(at);

        var oldAbcAngle = TriangleAngle(acLen, input.rootLength, input.tipLength);
        var newAbcAngle = TriangleAngle(atLen, input.rootLength, input.tipLength);

        // Bend normal strategy is to take whatever has been provided in the animation
        // stream to minimize configuration changes, however if this is collinear
        // try computing a bend normal given the desired target position.
        // If this also fails, try resolving axis using hint if provided.
        var axis = math.cross(ab, bc);
        var axisLengthSQ = math.lengthsq(axis);
        if (axisLengthSQ < k_SqrEpsilon)
        {
            axis = input.hasHint ? math.cross(input.hintPos - input.rootPos, bc) : float3.zero;

            if (axisLengthSQ < k_SqrEpsilon)
                axis = math.cross(at, bc);

            if (axisLengthSQ < k_SqrEpsilon)
                axis = math.up();
        }
        axis = math.normalize(axis);

        var a = 0.5f * (oldAbcAngle - newAbcAngle);
        var sin = math.sin(a);
        var cos = math.cos(a);
        var deltaR = new quaternion(axis.x * sin, axis.y * sin, axis.z * sin, cos);

        input.midRot = math.mul(deltaR, input.midRot);

        input.tipPos = input.midPos + math.rotate(input.midRot, math.up() * input.tipLength);
        ac = input.tipPos - input.rootPos;
        input.rootRot = math.mul(MathNew.FromToRotation(ac, at), input.rootRot);

        if (input.hasHint)
        {
            float acLengthSQ = math.lengthsq(ac);
            if (acLengthSQ > 0f)
            {
                var acNorm = ac / math.sqrt(acLengthSQ);
                var ah = input.hintPos - input.rootPos;
                var abProj = ab - acNorm * math.dot(ab, acNorm);
                var ahProj = ah - acNorm * math.dot(ah, acNorm);

                var maxReach = input.rootLength + input.tipLength;

                var abProjLengthSq = math.lengthsq(abProj);
                if (abProjLengthSq > (maxReach * maxReach * 0.001f) && abProjLengthSq > 0f)
                {
                    quaternion hintR = MathNew.FromToRotation(abProj, ahProj);
                    hintR.value.xyz *= input.hintWeight;
                    input.rootRot = math.mul(hintR, input.rootRot);
                }
            }
        }

        return new TwoBoneOutput { rootRot = input.rootRot, midRot = input.midRot, tipRot = tRotation };
    }

    static float TriangleAngle(float aLen, float aLen1, float aLen2)
    {
        return math.acos(math.clamp((aLen1 * aLen1 + aLen2 * aLen2 - aLen * aLen) / (aLen1 * aLen2) / 2.0f, -1.0f, 1.0f));
    }
}
