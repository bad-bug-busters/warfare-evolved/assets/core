using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateBefore(typeof(LocalPlayerInputSystem))]
public partial class LocalPlayerSetupSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<NetworkIdComponent>();
    }

    protected override void OnUpdate()
    {
        var localInput = SystemAPI.GetSingleton<CommandTargetComponent>().targetEntity;
        if (localInput == Entity.Null)
        {
            var ecb = new EntityCommandBuffer(Allocator.Temp);
            var localPlayerId = SystemAPI.GetSingleton<NetworkIdComponent>().Value;
            var commandTargetEntity = SystemAPI.GetSingletonEntity<CommandTargetComponent>();

            Entities.WithAll<PlayerTag>().WithNone<LocalPlayerTag>().ForEach((Entity entity, ref GhostOwnerComponent owner) =>
            {
                if (owner.NetworkId == localPlayerId)
                {
                    ecb.AddComponent<LocalPlayerTag>(entity);
                    ecb.SetComponent(commandTargetEntity, new CommandTargetComponent { targetEntity = entity });
                }
            }).Run();

            ecb.Playback(EntityManager);
        }
    }
}
