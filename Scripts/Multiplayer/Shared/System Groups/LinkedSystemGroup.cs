﻿using Unity.Entities;
using Unity.NetCode;

[WorldSystemFilter(WorldSystemFilterFlags.ServerSimulation | WorldSystemFilterFlags.ClientSimulation)]
[UpdateAfter(typeof(GhostSimulationSystemGroup))]
public class LinkedSystemGroup : ComponentSystemGroup
{
}
