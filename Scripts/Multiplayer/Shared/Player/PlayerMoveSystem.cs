//using Unity.Entities;
//using Unity.Jobs;
//using Unity.NetCode;
//using Unity.Mathematics;
//using Unity.Physics;
//using Unity.Physics.Extensions;
//using Unity.Physics.Systems;
//using Unity.Transforms;

//[DisableAutoCreation]
//[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
////[UpdateAfter(typeof(PlayerOffsetSystem))]

//public partial class PlayerMoveSystem : SystemBase
//{
//    BuildPhysicsWorld buildPhysicsWorld;
//    CollisionFilter filter;
//    SphereGeometry checkGeo;
//    SphereGeometry moveGeo;

//    protected override void OnCreate()
//    {
//        buildPhysicsWorld = World.GetOrCreateSystemManaged<BuildPhysicsWorld>();

//        filter = new CollisionFilter
//        {
//            BelongsTo = ~0u,
//            CollidesWith = 1u << 0,
//            GroupIndex = 0
//        };

//        checkGeo = new SphereGeometry()
//        {
//            Center = float3.zero,
//            Radius = 0.2f
//        };

//        moveGeo = new SphereGeometry()
//        {
//            Center = float3.zero,
//            Radius = 0.3f
//        };
//    }

//    protected override void OnUpdate()
//    {
//        var deltaTime = SystemAPI.Time.DeltaTime;
//        var world = buildPhysicsWorld.PhysicsWorld.CollisionWorld;
//        var filter = this.filter;
//        var checkGeo = this.checkGeo;
//        var moveGeo = this.moveGeo;

//        Entities.WithAny<InputPlayerNonVR, InputPlayerVRHead, InputPlayerVRHeadHands>().ForEach((ref PhysicsVelocity vel, ref Rotation rot, in Translation pos, in PhysicsMass mass, in PlayerMovement movement) =>
//        {
//            //rot.Value = targetRot.Value;

//            if (movement.moveX != 0 || movement.moveZ != 0 || (movement.jump && !movement.jumpLast))
//            {
//                var halfDist = new float3(0, 0.125f, 0);
//                var start = pos.Value + halfDist;
//                var end = pos.Value - halfDist;
//                var input = CastInput.Sphere(start, end, filter, checkGeo);

//                //check for support
//                if (world.CastCollider(input))
//                {
//                    var impulse = float3.zero;
//                    if (movement.moveX != 0 || movement.moveZ != 0)
//                    {
//                        impulse = math.rotate(movement.heading, new float3(movement.moveX, 0, movement.moveZ));
//                        impulse = math.normalize(impulse);

//                        //check normal
//                        input = CastInput.Sphere(start, end, filter, moveGeo);
//                        ColliderCastHit hit;
//                        if (world.CastCollider(input, out hit))
//                        {
//                            var slope = -math.dot(impulse, hit.SurfaceNormal);
//                            if (slope > 0)
//                                impulse.y = slope * 2;
//                        }
//                        impulse *= movement.sprint ? 50 : 25;
//                        impulse /= math.length(vel.Linear) + 1;
//                    }

//                    if (movement.jump && !movement.jumpLast)
//                        impulse.y += 300;

//                    vel.ApplyLinearImpulse(mass, impulse);
//                }
//            }
//        }).ScheduleParallel();

//        //CompleteDependency();
//    }
//}
