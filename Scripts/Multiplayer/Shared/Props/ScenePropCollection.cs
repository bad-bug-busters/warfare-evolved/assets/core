﻿using Unity.Entities;
using UnityEngine;
using System;

[Serializable]
public struct ScenePropCollection : IBufferElementData
{
    public Entity Value;

    public static implicit operator Entity(ScenePropCollection e)
    {
        return e.Value;
    }

    public static implicit operator ScenePropCollection(Entity e)
    {
        return new ScenePropCollection { Value = e };
    }
}

public class ScenePropCollectionAuthoring : MonoBehaviour
{
    public ScenePropCollection root;
}

public class ScenePropCollectionBaker : Baker<ScenePropCollectionAuthoring>
{
    public override void Bake(ScenePropCollectionAuthoring authoring)
    {
        AddBuffer<ScenePropCollection>(authoring.root);
    }
}
