using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;
using UnityEditor;

[ExecuteAlways]
public class RoundWallBuilder : MonoBehaviour
{
    public bool build;
    public bool cleanup;
    public GameObject prefab;
    public int instances = 8;
    public float radius = 1;
    [Range(0, 360)] public float buildAngle = 360;
    [Range(-180, 180)] public float startAngle = 0;
    public Vector3 scale = Vector3.one;

    void OnValidate()
    {
        if (build)
        {
            cleanup = true;
        }
    }

    void Update()
    {
        if (cleanup)
        {
            if (transform.childCount == 0)
            {
                cleanup = false;
            }
            else
            {
                foreach (Transform child in transform)
                {
                    DestroyImmediate(child.gameObject);
                }
            }
        }
        else if (build)
        {
            build = false;

            var stepAngle = buildAngle / instances;
            for (int i = 0; i < instances; i++)
            {
                var rot = Quaternion.Euler(new Vector3(0, startAngle + i * stepAngle, 0));
                var pos = rot * new Vector3(0, 0, radius);

                var instance = PrefabUtility.InstantiatePrefab(prefab, transform) as GameObject;

                instance.transform.localPosition = pos;
                instance.transform.localRotation = rot;
                instance.transform.localScale = scale;

                ShapeVariation shapeVar;
                if (instance.TryGetComponent(out shapeVar))
                {
                    shapeVar.OnValidate();
                }
            }
        }
    }
}
