using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
public partial class InputGeneralSystem : SystemBase
{
    protected override void OnCreate()
    {
        EntityManager.CreateEntity(typeof(InputGeneral));
        RequireForUpdate<InputGeneral>();
    }

    protected override void OnUpdate()
    {
        var input = SystemAPI.GetSingleton<InputGeneral>();

        input.moveX = Convert.ToSByte(Input.GetAxis("Move Horizontal") * sbyte.MaxValue);
        input.moveZ = Convert.ToSByte(Input.GetAxis("Move Vertical") * sbyte.MaxValue);
        input.rotY = Convert.ToSByte(math.clamp(Input.GetAxis("Rotate Horizontal"), -1, 1) * sbyte.MaxValue);
        input.sprint = Input.GetButton("Sprint");
        input.jump = Input.GetButton("Jump");

        SystemAPI.SetSingleton(input);
    }
}
