using Unity.Entities;
using Unity.NetCode;

//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateAfter(typeof(LocalPlayerSetupSystem))]
//[UpdateBefore(typeof(PlayerMoveSystem))]
public partial class LocalPlayerInputSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<LocalPlayerTag>();
        RequireForUpdate<NetworkIdComponent>();
        RequireForUpdate<SettingsDataVR>();
    }

    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;
        //UnityEngine.Debug.Log("i:" + tick);
        var localPlayerAgent = SystemAPI.GetSingletonEntity<LocalPlayerTag>();

        var vrSettings = SystemAPI.GetSingleton<SettingsDataVR>();

        if (vrSettings.enableVR && vrSettings.useHandControllers)
        {
            if (!EntityManager.HasBuffer<InputPlayerVRHeadHands>(localPlayerAgent))
            {
                EntityManager.AddBuffer<InputPlayerVRHeadHands>(localPlayerAgent);
                return;
            }
            var input = default(InputPlayerVRHeadHands);
            input.Tick = tick;

            input.general = SystemAPI.GetSingleton<InputGeneral>();
            input.VRHead = SystemAPI.GetSingleton<InputVRHead>();
            input.VRHands = SystemAPI.GetSingleton<InputVRHands>();

            var inputBuffer = EntityManager.GetBuffer<InputPlayerVRHeadHands>(localPlayerAgent);
            inputBuffer.AddCommandData(input);
        }
        else if (vrSettings.enableVR && !vrSettings.useHandControllers)
        {
            if (!EntityManager.HasBuffer<InputPlayerVRHead>(localPlayerAgent))
            {
                EntityManager.AddBuffer<InputPlayerVRHead>(localPlayerAgent);
                return;
            }
            var input = default(InputPlayerVRHead);
            input.Tick = tick;

            input.general = SystemAPI.GetSingleton<InputGeneral>();
            input.VRHead = SystemAPI.GetSingleton<InputVRHead>();
            input.noVRHands = SystemAPI.GetSingleton<InputNoVRHands>();

            var inputBuffer = EntityManager.GetBuffer<InputPlayerVRHead>(localPlayerAgent);
            inputBuffer.AddCommandData(input);
        }
        else
        {
            if (!EntityManager.HasBuffer<InputPlayerNonVR>(localPlayerAgent))
            {
                EntityManager.AddBuffer<InputPlayerNonVR>(localPlayerAgent);
                return;
            }
            var input = default(InputPlayerNonVR);
            input.Tick = tick;

            input.general = SystemAPI.GetSingleton<InputGeneral>();
            input.noVRHead = SystemAPI.GetSingleton<InputNoVRHead>();
            input.noVRHands = SystemAPI.GetSingleton<InputNoVRHands>();

            var inputBuffer = EntityManager.GetBuffer<InputPlayerNonVR>(localPlayerAgent);
            inputBuffer.AddCommandData(input);
        }
    }
}