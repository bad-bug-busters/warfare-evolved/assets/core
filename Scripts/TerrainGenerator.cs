﻿using System.Collections;
using System.Collections.Generic;
using System.Linq; // used for Sum of array
using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

public class TerrainGenerator : MonoBehaviour
{
    public bool generate;
    public bool island;
    [Range(1, 16)] public int octaves = 4;
    [Range(0.01f, 16)] public float frequency = 1;
    [Range(0, 1)] public float persistance = 0.5f;
    [Range(1, 16)] public float lacunarity = 2f;

    bool firstTime = true;
    TerrainData tData;

    void OnValidate()
    {
        if (firstTime)
        {
            tData = GetComponent<Terrain>().terrainData;
            firstTime = false;
        }

        if (!generate)
            return;

        var size = tData.heightmapResolution;
        var maxHeight = 0f;
        var amplitude = 1f;
        for (int i = 0; i < octaves; i++)
        {
            maxHeight += amplitude;
            amplitude *= persistance;
        }

        NativeArray<float> noiseData = new NativeArray<float>(size * size, Allocator.TempJob);

        var job = new NoiseJob()
        {
            island = island,
            octaves = octaves,
            frequency = frequency,
            persistance = persistance,
            lacunarity = lacunarity,
            maxHeight = maxHeight,
            size = size,
            noiseData = noiseData
        };
        var handle = job.Schedule(noiseData.Length, 1);
        handle.Complete();

        float[,] heightMap = new float[size, size];
        for (int i = 0; i < noiseData.Length; i++)
        {
            var x = i % size;
            var y = i / size;
            heightMap[x, y] = noiseData[i];
            //Debug.Log("height:" + noiseData[i].x + " ad:" + noiseData[i].y + "/" + noiseData[i].z);
        }

        tData.SetHeights(0, 0, heightMap);
        noiseData.Dispose();
    }
}

[BurstCompile]
public struct NoiseJob : IJobParallelFor
{
    public bool island;
    public int octaves;
    public float frequency;
    public float persistance;
    public float lacunarity;
    public float maxHeight;
    public int size;
    [WriteOnly] public NativeArray<float> noiseData;

    public void Execute(int i)
    {
        var p = 1f;
        var f = frequency;
        var height = 0f;
        var gradient = float2.zero;

        var x = i % size;
        var y = i / size;
        for (int j = 0; j < octaves; j++)
        {
            var noiseCur = noise.srdnoise(new float2(x, y) / size * f);
            gradient += noiseCur.yz;
            height += p * (noiseCur.x + 0.5f) / (1 + math.dot(gradient, gradient));
            p *= persistance;
            f *= lacunarity;
        }
        height = (height * 0.5f + 0.25f) / maxHeight;

        if (island)
        {
            //var factor = math.abs(2f / size * new float2(x, y) - 1);
            //height *= 1 - math.pow(math.max(factor.x, factor.y), 1);
            var factor = math.abs(2f / size * new float2(x, y) - 1);
            var dist = math.length(factor);
            var maxDist = math.max(factor.x, factor.y);
            height *= 1 - math.lerp(dist, maxDist, maxDist);
        }
        noiseData[i] = height;
    }
}
