using System.Collections;
using System.Collections.Generic;
using Unity.Deformations;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;
using UnityEngine;

//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
[UpdateAfter(typeof(TransformSystemGroup))]
//[UpdateAfter(typeof(PlayerAnimationSystem))]
public partial class SkinMatrixSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var brBfe = GetBufferLookup<BoneReference>(true);
        Entities
            .WithReadOnly(brBfe)
            .ForEach((Entity entity, ref DynamicBuffer<SkinMatrix> skinMatrices, in BindSkeletonRoot root) =>
        {
            if (brBfe.HasBuffer(root.Value))
            {
                var bones = brBfe[root.Value];
                if (bones.Length != skinMatrices.Length)
                {
                    //Debug.Log("enity: "+ entity + "bones: " + bones.Length + " skin: " + skinMatrices.Length);
                    return;
                }

                var rootToWorld = SystemAPI.GetComponent<LocalToWorld>(root.Value);
                var worldToRoot = math.inverse(rootToWorld.Value);

                for (int i = 0; i < bones.Length; i++)
                {
                    var bindPose = SystemAPI.GetComponent<BoneBindPose>(bones[i].Value);
                    var boneToWorld = SystemAPI.GetComponent<LocalToWorld>(bones[i].Value).Value;
                    var boneToRoot = math.mul(worldToRoot, boneToWorld);
                    var skinMat = math.mul(boneToRoot, bindPose.Value);
                    skinMatrices[i] = new SkinMatrix { Value = new float3x4(skinMat.c0.xyz, skinMat.c1.xyz, skinMat.c2.xyz, skinMat.c3.xyz) };
                }
            }
        }).ScheduleParallel();
    }
}
