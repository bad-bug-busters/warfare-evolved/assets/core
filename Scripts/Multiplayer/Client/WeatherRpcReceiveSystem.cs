//using Unity.Entities;
//using Unity.NetCode;

//[DisableAutoCreation]
//[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
//[UpdateInGroup(typeof(RpcReceiveSystemGroup))]
//public partial class WeatherRpcReceiveSystem : SystemBase
//{
//    protected override void OnUpdate()
//    {
//        Entities.ForEach((Entity reqEnt, ref WeatherRpc req, ref ReceiveRpcCommandRequestComponent reqSrc) =>
//        {
//            UnityEngine.Debug.Log("fog:" + req.weather.fogDistance + " clouds:" + req.weather.clouds + " wind:" + req.weather.wind);
//            SystemAPI.SetSingleton(req.weather);
//            EntityManager.DestroyEntity(reqEnt);
//        }).WithStructuralChanges().Run();
//    }
//}
