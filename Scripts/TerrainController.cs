﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class gSettings
{
    [Range(1, 64)] public int scale = 8;
    [Range(1, 64)] public int octaves = 2;
    [Range(0, 1)] public float persistance = 0.5f;
    /*[Range(1, 1000)]*/
    public float lacunarity = 2f;
    public int seed = 1;
    public Vector2 offset;

    [Range(0, 16)] public float warping = 0f;
    public bool ridged = false;
    public bool valley = false;
}

[System.Serializable]
public class eSettings
{
    public int ErosionIterations = 50000;
}

[System.Serializable]
public class pSettings
{
    public bool layerNoise = true;

    static int numLayers = 8;
    public bool[] applyLayer = new bool[numLayers];

    //public int dropletsSquared = 4;
    //public int dropletsLife = 16;

    public Vector3[] layerExponentValues = new Vector3[numLayers];
}


public class TerrainController : MonoBehaviour
{
    //public Vector3 terrainSize = new Vector3(512, 128, 512);

    [Header("Terrain Application")]
    public bool generate;
    public bool erode;
    public bool paint;
    public bool populate;

    [Header("Terrain Settings")]
    public gSettings generation;
    public pSettings painting;
    public eSettings erosion;

    bool firstTime = true;
    TerrainData tData;

    TerrainErosion tErosion;

    void OnValidate()
    {
        if (firstTime)
        {
            tData = GetComponent<Terrain>().terrainData;
            tErosion = GetComponent<TerrainErosion>();
            firstTime = false;
        }

        //tData.size = terrainSize;

        if (generate)
        {
            tData.SetHeights(0, 0, TerrainTls.Generate(generation, tData.heightmapResolution, tData.heightmapResolution));
        }

        if (erode)
        {

        }

        if (paint)
        {
            tData.SetAlphamaps(0, 0, TerrainTls.Paint(painting, tData));
        }
    }
}

