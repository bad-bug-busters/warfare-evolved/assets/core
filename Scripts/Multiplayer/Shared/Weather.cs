﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[System.Serializable]

public struct Weather : IComponentData
{
    [Range(1, 16384)] public float fogDistance;
    [Range(0, 1)] public float clouds;
    public float2 wind;
}
