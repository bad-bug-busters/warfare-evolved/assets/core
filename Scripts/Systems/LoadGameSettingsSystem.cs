using System;
using Unity.Entities;
using Unity.NetCode;

[Serializable]
public struct SettingsDataVR : IComponentData
{
    public bool enableVR;
    public bool gripToggle;
    public bool seated;
    public bool useHandControllers;
    public byte bodyHeight;
}

//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
[UpdateInGroup(typeof(InitializationSystemGroup))]
public partial class LoadGameSettingsSystem : SystemBase
{
    struct LoadGameSettings : IComponentData
    {
    }

    protected override void OnCreate()
    {
        RequireForUpdate<LoadGameSettings>();
        EntityManager.CreateEntity(typeof(LoadGameSettings));
        SettingsVR.LoadFile();
        SettingsVR.LoadXR();
        var entity = EntityManager.CreateEntity(typeof(SettingsDataVR));
        EntityManager.SetComponentData(entity, SettingsVR.Data);
        UnityEngine.Debug.Log("settings VR: " + entity.Index);
    }

    protected override void OnUpdate()
    {
        SettingsVR.SetXR();
        EntityManager.DestroyEntity(SystemAPI.GetSingletonEntity<LoadGameSettings>());
    }
}
