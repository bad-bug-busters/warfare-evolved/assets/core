using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateBefore(typeof(InputGeneralSystem))]

public partial class InputNoVRHeadSystem : SystemBase
{
    protected override void OnCreate()
    {
        if (!SettingsVR.curData.enableVR)
        {
            EntityManager.CreateEntity(typeof(InputNoVRHead));
        }
        RequireForUpdate<InputNoVRHead>();
    }

    protected override void OnUpdate()
    {
        var input = SystemAPI.GetSingleton<InputNoVRHead>();

        input.rotX = Convert.ToSByte(math.clamp(Input.GetAxis("Rotate Vertical"), -1, 1) * sbyte.MaxValue);
        input.freeLook = Input.GetButton("Free Look");

        SystemAPI.SetSingleton(input);
    }
}
