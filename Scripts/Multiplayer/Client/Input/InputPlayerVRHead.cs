﻿using Unity.NetCode;

public struct InputPlayerVRHead : ICommandData
{
    public NetworkTick Tick {get; set;}
    public InputGeneral general;
    public InputVRHead VRHead;
    public InputNoVRHands noVRHands;
}
