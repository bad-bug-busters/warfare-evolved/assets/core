﻿using Unity.Entities;

public struct InputNoVRHead : IComponentData
{
    public sbyte rotX;
    public bool freeLook;
}
