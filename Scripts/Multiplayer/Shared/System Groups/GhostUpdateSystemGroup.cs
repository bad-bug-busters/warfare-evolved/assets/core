using Unity.Entities;
using Unity.NetCode;

[UpdateInGroup(typeof(GhostSimulationSystemGroup))]
[UpdateAfter(typeof(PredictedSimulationSystemGroup))]
public class GhostUpdateSystemGroup : ComponentSystemGroup
{
}
