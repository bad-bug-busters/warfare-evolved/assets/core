using Unity.Entities;
using Unity.NetCode;

//[DisableAutoCreation]
[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
public partial class PlayerVRHandsSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;

        Entities
            .ForEach((DynamicBuffer<InputPlayerVRHeadHands> buffer, ref PlayerVRHands VRHands, in PredictedGhostComponent prediction) =>
        {
            buffer.GetDataAtTick(tick, out InputPlayerVRHeadHands input);
            for (int i = 0; i < 2; i++)
            {
                VRHands.pos[i] = input.VRHands.pos[i];
                VRHands.rot[i] = input.VRHands.rot[i];
                VRHands.trigger[i] = input.VRHands.trigger[i];
                VRHands.grip[i] = input.VRHands.grip[i];
            }
        })
            .ScheduleParallel();
    }
}
