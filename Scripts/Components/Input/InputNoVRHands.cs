﻿using Unity.Entities;

public struct InputNoVRHands : IComponentData
{
    public bool primaryAttack;
    public bool secondaryAttack;
    public bool block;
    public bool offhandAttack;
    public bool interact;
    public bool drop;
}
