﻿using UnityEngine;
using UnityEditor;

[ExecuteAlways]
public class EditorCamera : MonoBehaviour
{
    Transform sceneCameraTrans;

    void LateUpdate()
    {
        if (sceneCameraTrans == null)
            sceneCameraTrans = SceneView.lastActiveSceneView.camera.transform;
        else
        {
            transform.position = sceneCameraTrans.position;
            transform.rotation = sceneCameraTrans.rotation;
        }
    }
}
