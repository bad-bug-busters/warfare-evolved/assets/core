﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using UnityEngine;

[Serializable]
[MaterialProperty("_BaseColorMap_ST")]
public struct TilingOffset : IComponentData
{
    public float4 Value;
}

public class TilingOffsetAuthoring : MonoBehaviour
{
    public TilingOffset tilingOffset;
}

public class TilingOffsetBaker : Baker<TilingOffsetAuthoring>
{
    public override void Bake(TilingOffsetAuthoring authoring)
    {
        AddComponent(authoring.tilingOffset);
    }
}
