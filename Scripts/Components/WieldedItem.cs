﻿using Unity.Entities;


public struct WieldedItem : IComponentData
{
    public Entity LeftHand;
    public Entity RightHand;
}
