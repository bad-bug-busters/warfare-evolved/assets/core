using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;

//[DisableAutoCreation]
[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
public partial class PlayerVRHeadSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;

        var inputPlayerVRHeadData = GetBufferLookup<InputPlayerVRHead>(true);
        var inputPlayerVRHeadHandsData = GetBufferLookup<InputPlayerVRHeadHands>(true);

        Entities
            .WithReadOnly(inputPlayerVRHeadData)
            .WithReadOnly(inputPlayerVRHeadHandsData)
            .WithAny<InputPlayerVRHead, InputPlayerVRHeadHands>()
            .ForEach((Entity entity, ref PlayerVRHead VRHead, in PredictedGhostComponent prediction) =>
        {
            InputVRHead inputVRHead;
            if (inputPlayerVRHeadData.HasBuffer(entity))
            {
                var buffer = inputPlayerVRHeadData[entity];
                buffer.GetDataAtTick(tick, out InputPlayerVRHead input);
                inputVRHead = input.VRHead;
            }
            else
            {
                var buffer = inputPlayerVRHeadHandsData[entity];
                buffer.GetDataAtTick(tick, out InputPlayerVRHeadHands input);
                inputVRHead = input.VRHead;
            }
            VRHead.pos = inputVRHead.pos;
            VRHead.rot = inputVRHead.rot;
        })
            .ScheduleParallel();

    }
}
