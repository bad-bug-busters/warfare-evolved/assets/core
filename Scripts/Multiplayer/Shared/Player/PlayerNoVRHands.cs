﻿using Unity.Entities;
using Unity.NetCode;


public struct PlayerNoVRHands : IComponentData
{
    [GhostField] public bool interact;
    public bool interactLast;
    [GhostField] public bool drop;
    public bool dropLast;
}
