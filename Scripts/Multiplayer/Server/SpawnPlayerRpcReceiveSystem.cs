using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using Unity.Transforms;

// When server receives go in game request, go in game and delete request
//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ServerSimulation)]
[UpdateInGroup(typeof(RpcReceiveSystemGroup))]
public partial class SpawnPlayerRpcReceiveSystem : SystemBase
{
    Random random;
    protected override void OnCreate()
    {
        RequireForUpdate(GetEntityQuery(ComponentType.ReadOnly<SpawnPlayerRpc>(), ComponentType.ReadOnly<ReceiveRpcCommandRequestComponent>()));
        RequireForUpdate<PlayerSpawner>();
        RequireForUpdate<SpawnPointCollection>();
        random = new Random(1);
    }

    protected override void OnUpdate()
    {
        var spawnPointCollection = SystemAPI.GetSingletonEntity<SpawnPointCollection>();
        var spawnPoints = EntityManager.GetBuffer<Child>(spawnPointCollection);

        var prefabs = SystemAPI.GetSingleton<PlayerSpawner>();
        var playerPrefabs = new NativeArray<Entity>(3, Allocator.Temp);
        playerPrefabs[0] = prefabs.player;
        playerPrefabs[1] = prefabs.playerHMD;
        playerPrefabs[2] = prefabs.playerVR;

        var ecb = new EntityCommandBuffer(Allocator.Temp);
        var networkIdFromEntity = GetComponentLookup<NetworkIdComponent>(true);

        Entities.WithReadOnly(networkIdFromEntity).ForEach((Entity reqEnt, in SpawnPlayerRpc req, in ReceiveRpcCommandRequestComponent reqSrc) =>
        {
            ecb.AddComponent<NetworkStreamInGame>(reqSrc.SourceConnection);
            var networkId = networkIdFromEntity[reqSrc.SourceConnection].Value;

            var prefab = playerPrefabs[req.inputMode];
            var entity = ecb.Instantiate(prefab);

            ecb.SetComponent(entity, new GhostOwnerComponent { NetworkId = networkId });

            // Add the player to the linked entity group so it is destroyed automatically on disconnect
            ecb.AppendToBuffer(reqSrc.SourceConnection, new LinkedEntityGroup { Value = entity });

            var id = random.NextInt(spawnPoints.Length);
            var spawnTrans = EntityManager.GetComponentData<LocalTransform>(spawnPoints[id].Value);
            spawnTrans.Position += new float3(random.NextFloat(-1, 1), 0, random.NextFloat(-1, 1));
            ecb.SetComponent(entity, spawnTrans);

            UnityEngine.Debug.Log("Player requested spawn (ID: " + networkId + ", InputMode: " + req.inputMode + ", Spawnpoint: " + id + ")");

            if (req.inputMode == 0)
                ecb.AddBuffer<InputPlayerNonVR>(entity);
            else if (req.inputMode == 1)
                ecb.AddBuffer<InputPlayerVRHead>(entity);
            else
                ecb.AddBuffer<InputPlayerVRHeadHands>(entity);

            ecb.SetComponent(reqSrc.SourceConnection, new CommandTargetComponent { targetEntity = entity });

            ////send daytime
            //var reqClient = ecb.CreateEntity();
            //ecb.AddComponent(reqClient, new DayTimeRpc { dayTime = SystemAPI.GetSingleton<DayTime>() });
            //ecb.AddComponent(reqClient, new SendRpcCommandRequestComponent { TargetConnection = reqSrc.SourceConnection });

            ////send weather
            //reqClient = ecb.CreateEntity();
            //ecb.AddComponent(reqClient, new WeatherRpc { weather = SystemAPI.GetSingleton<Weather>() });
            //ecb.AddComponent(reqClient, new SendRpcCommandRequestComponent { TargetConnection = reqSrc.SourceConnection });

            ecb.DestroyEntity(reqEnt);
        }).WithoutBurst().Run();

        ecb.Playback(EntityManager);
        //ecb.Dispose();
        playerPrefabs.Dispose();
    }
}
