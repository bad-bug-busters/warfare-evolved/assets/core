﻿//using Unity.Entities;
//using Unity.Physics;
//using Unity.Physics.Systems;
//using Unity.Mathematics;
//using Unity.NetCode;
//using Unity.Transforms;
//using UnityEngine;
//using RaycastHit = Unity.Physics.RaycastHit;

//[DisableAutoCreation]
//[WorldSystemFilter(WorldSystemFilterFlags.ServerSimulation)]
//[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
//public class DestructionSystem : SystemBase
//{
//    BuildPhysicsWorld buildPhysicsWorld;
//    CollisionFilter collisionFilter;

//    protected override void OnCreate()
//    {
//        buildPhysicsWorld = World.GetOrCreateSystemManaged<BuildPhysicsWorld>();

//        collisionFilter = new CollisionFilter()
//        {
//            BelongsTo = ~0u,
//            CollidesWith = 1u << 0,
//            GroupIndex = 0
//        };
//    }

//    protected override void OnUpdate()
//    {
//        var world = buildPhysicsWorld.PhysicsWorld.CollisionWorld;
//        var filter = collisionFilter;

//        Entities.ForEach((ref PlayerNoVRHead noVRHead, ref PlayerNoVRHands noVRHands, ref Rotation pRot, ref Translation pPos) =>
//        {
//            if (noVRHands.interact && !noVRHands.interactLast)
//            {
//                var start = pPos.Value + math.rotate(pRot.Value, new float3(0, noVRHead.headHeight, 0));
//                var end = start + math.rotate(math.mul(pRot.Value, quaternion.Euler(new float3(noVRHead.headRot, 0))), new float3(0, 0, 10));
//                var input = CastInput.Ray(start, end, filter);

//                RaycastHit hit;
//                if (world.CastRay(input, out hit))
//                {
//                    var hitEnt = world.Bodies[hit.RigidBodyIndex].Entity;
//                    if (EntityManager.HasBuffer<FracPrefab>(hitEnt))
//                    {
//                        var id = EntityManager.GetComponentData<SceneProp>(hitEnt).id;
//                        var newEvent = EntityManager.CreateEntity();
//                        EntityManager.AddComponentData(newEvent, new ReplacePropEvent { id = id, isServer = true, hitPos = hit.Position });

//                        //broadcast
//                        Entities.WithAll<NetworkStreamInGame>().ForEach((Entity ent, ref NetworkIdComponent netId) =>
//                        {
//                            var reqEnt = EntityManager.CreateEntity();
//                            EntityManager.AddComponentData(reqEnt, new ReplacePropRpc { id = id });
//                            EntityManager.AddComponentData(reqEnt, new SendRpcCommandRequestComponent { TargetConnection = ent });
//                        });
//                    }
//                    else if (EntityManager.HasBuffer<HitPoints>(hitEnt))
//                    {
//                        var id = EntityManager.GetComponentData<SceneProp>(hitEnt).id;
//                        if (HandleHit(hitEnt, id, EntityManager))
//                        {
//                            //broadcast
//                            Entities.WithAll<NetworkStreamInGame>().ForEach((Entity ent, ref NetworkIdComponent netId) =>
//                            {
//                                var reqEnt = EntityManager.CreateEntity();
//                                EntityManager.AddComponentData(reqEnt, new DestroyPropRpc { id = id });
//                                EntityManager.AddComponentData(reqEnt, new SendRpcCommandRequestComponent { TargetConnection = ent });
//                            });
//                        }
//                    }
//                }
//            }
//        });
//    }

//    public static bool HandleHit(Entity hitEnt, ushort id, EntityManager em)
//    {
//        var hp = em.GetComponentData<HitPoints>(hitEnt);
//        hp.Value -= 50;
//        Debug.Log("hp: " + hp.Value);
//        if (hp.Value > 0)
//        {
//            em.SetComponentData(hitEnt, hp);
//            return false;
//        }
//        else
//        {
//            var newEvent = em.CreateEntity();
//            em.AddComponentData(newEvent, new DestroyPropEvent { id = id, isServer = true });
//            return true;
//        }
//    }
//}
