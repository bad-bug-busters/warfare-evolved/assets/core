﻿using UnityEngine;

[CreateAssetMenu(fileName = "AudioSource", menuName = "ScriptableObjects/AudioSource", order = 1)]
public class AudioSourceSO : ScriptableObject
{
    public AudioClip[] audioClips;
}
