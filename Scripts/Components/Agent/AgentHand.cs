﻿using Unity.Entities;


public struct AgentHand : IComponentData
{
    public Entity leftHand;
    public Entity rightHand;
}
