using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;
using System;

[Serializable, VolumeComponentMenu("Post-processing/Custom/HeatHaze")]
public sealed class HeatHaze : CustomPostProcessVolumeComponent, IPostProcessComponent
{
    [Tooltip("Controls the intensity of the effect.")]
    public ClampedFloatParameter intensity = new ClampedFloatParameter(0f, 0f, 1f);

    public Vector2Parameter distanceHeight = new Vector2Parameter(new Vector2(55261.97f, 55261.97f));

    public Vector4Parameter frequencyScale = new Vector4Parameter(new Vector4(2, 1, 64, 32));

    public ClampedFloatParameter layerMix = new ClampedFloatParameter(0.5f, 0f, 1f);

    public TextureParameter distortionCubeMap = new TextureParameter(null);

    public Vector4Parameter debugMap = new Vector4Parameter(new Vector4(1, 1, 1, 0));

    Material m_Material;

    public bool IsActive() => m_Material != null && intensity.value > 0f && distortionCubeMap != null;

    // Do not forget to add this post process in the Custom Post Process Orders list (Project Settings > HDRP Default Settings).
    public override CustomPostProcessInjectionPoint injectionPoint => CustomPostProcessInjectionPoint.AfterOpaqueAndSky;

    const string kShaderName = "Hidden/Shader/HeatHaze";

    public override void Setup()
    {
        if (Shader.Find(kShaderName) != null)
            m_Material = new Material(Shader.Find(kShaderName));
        else
            Debug.LogError($"Unable to find shader '{kShaderName}'. Post Process Volume HeatHaze is unable to load.");
    }

    public override void Render(CommandBuffer cmd, HDCamera camera, RTHandle source, RTHandle destination)
    {
        if (m_Material == null)
            return;

        m_Material.SetFloat("_Intensity", intensity.value);
        m_Material.SetVector("_DistanceHeight", distanceHeight.value);
        m_Material.SetVector("_FrequencyScale", frequencyScale.value);
        m_Material.SetFloat("_LayerMix", layerMix.value);
        m_Material.SetTexture("_DistortionCubeMap", distortionCubeMap.value);
        m_Material.SetVector("_DebugMap", debugMap.value);

        m_Material.SetFloat("_FOV", camera.camera.fieldOfView);

        m_Material.SetVector("_CamPos", ShaderConfig.s_CameraRelativeRendering != 0 ? camera.camera.transform.position : Vector3.zero);

        m_Material.SetTexture("_InputTexture", source);
        HDUtils.DrawFullScreen(cmd, m_Material, destination);
    }

    public override void Cleanup()
    {
        CoreUtils.Destroy(m_Material);
    }
}
