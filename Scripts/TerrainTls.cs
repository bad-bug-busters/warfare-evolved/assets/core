﻿using System.Collections;
using System.Collections.Generic;
using System.Linq; // used for Sum of array
using Unity.Mathematics;
using UnityEngine;

public static class TerrainTls
{
    public static float[,] Generate(/*int scale, float persistance, float lacunarity, int seed, Vector2 offset, bool ridged, bool valley*/gSettings gS, int mapWidth, int mapHeight)
    {
        float[,] noiseMap = new float[mapWidth, mapHeight];

        int powTwoScale = (int)math.pow(gS.ridged ? gS.scale * 2 : gS.scale, 2);

        int octaves = gS.octaves;//preserve terrain detail

        System.Random prng = new System.Random(gS.seed);
        float2[] octaveOffsets = new float2[octaves];

        float maxPossibleHeight = 0;
        float amplitude = 1;
        float frequency = 1;

        for (int i = 0; i < octaves; i++)
        {
            octaveOffsets[i] = new float2(prng.Next(-100000, 100000) + gS.offset.x, prng.Next(-100000, 100000) + gS.offset.y);

            maxPossibleHeight += amplitude;
            amplitude *= gS.persistance;
        }

        float maxLocalNoiseHeight = float.MinValue;
        float minLocalNoiseHeight = float.MaxValue;

        float halfWidth = mapWidth / 2f;
        float halfHeight = mapHeight / 2f;

        for (int x = 0; x < mapHeight; x++)
        {
            for (int y = 0; y < mapWidth; y++)
            {
                amplitude = 1;
                frequency = 1;
                float noiseHeight = 0;

                for (int i = 0; i < octaves; i++)
                {
                    float2 sample = new float2(x - halfWidth + octaveOffsets[i].x, y - halfHeight + octaveOffsets[i].y) / powTwoScale * frequency;

                    float perlinValue;

                    if (gS.warping != 0f)
                    {
                        float warp = noise.snoise(sample) * gS.warping * amplitude;
                        perlinValue = noise.snoise(sample + warp);
                    }
                    else
                    {
                        perlinValue = noise.snoise(sample);
                    }

                    if (gS.ridged)
                    {
                        perlinValue = math.abs(0.5f - math.abs(0.5f - perlinValue) * 2f);
                    }

                    noiseHeight += perlinValue * amplitude;

                    amplitude *= gS.persistance;
                    frequency *= gS.lacunarity;
                }

                if (noiseHeight > maxLocalNoiseHeight)
                {
                    maxLocalNoiseHeight = noiseHeight;
                }
                if (noiseHeight < minLocalNoiseHeight)
                {
                    minLocalNoiseHeight = noiseHeight;
                }
                noiseMap[x, y] = noiseHeight;
            }
        }

        //normalize the result
        for (int x = 0; x < mapHeight; x++)
        {
            for (int y = 0; y < mapWidth; y++)
            {
                noiseMap[x, y] = Mathf.InverseLerp(minLocalNoiseHeight, maxLocalNoiseHeight, noiseMap[x, y]);

                if (gS.valley)
                {
                    noiseMap[x, y] = math.pow(noiseMap[x, y], 2);
                }
            }
        }

        return noiseMap;
    }




    public static float[,,] Paint(pSettings pS, TerrainData tData)
    {
        Vector2Int mapSize = new Vector2Int(tData.alphamapWidth, tData.alphamapHeight);
        //mapSize.x = 2;
        //mapSize.y = 2;

        //int[,] wetMap = new int[mapSize.x, mapSize.y];
        //// For each point on the alphamap...
        //for (int x = 0; x < pS.dropletsSquared; x++)
        //{
        //    for (int y = 0; y < pS.dropletsSquared; y++)
        //    {
        //        int curX = x * mapSize.x / pS.dropletsSquared;
        //        int curY = y * mapSize.y / pS.dropletsSquared;
        //        float height = tData.GetHeight(curY, curX);
        //        float smallestHeight = height;

        //        Vector2Int[] offset = new Vector2Int[4];
        //        offset[0] = new Vector2Int(0, 1);
        //        offset[1] = new Vector2Int(0, -1);
        //        offset[2] = new Vector2Int(-1, 0);
        //        offset[3] = new Vector2Int(1, 0);

        //        float[] heights = new float[4];

        //        //while (smallestHeight <= height)
        //        for (int j = 0; j < pS.dropletsLife; j++)
        //        {
        //            Debug.Log("@@@ x:" + curX + " y:" + curY + " life:" + j + " height:" + height);

        //            Vector2Int nextCheckPos = Vector2Int.zero;

        //            // check for smallest height to define flow direction
        //            for (int i = 0; i < heights.Length; i++)
        //            {
        //                Vector2Int checkPos = new Vector2Int(curY + offset[i].y, curX + offset[i].x);
        //                // dont check beyond map boundaries
        //                if (checkPos.x > -1 && checkPos.x < mapSize.x && checkPos.y > -1 && checkPos.y < mapSize.y)
        //                {
        //                    heights[i] = tData.GetHeight(checkPos.y, checkPos.x);
        //                    if (heights[i] < smallestHeight)
        //                    {
        //                        smallestHeight = heights[i];
        //                        nextCheckPos = checkPos;
        //                        Debug.Log("smallest:" + checkPos);
        //                    }
        //                }
        //            }
        //            // found new smallest height so keep flowing
        //            if (smallestHeight < height)
        //            {
        //                height = smallestHeight;
        //                curX = nextCheckPos.x;
        //                curY = nextCheckPos.y;
        //                wetMap[curX, curY]++;// add wetness to flow direction
        //                Debug.Log(curX + ":" + curY + " wetness:" + wetMap[curX, curY]);
        //            }
        //            else
        //            {
        //                Debug.Log("####################################");
        //                break;
        //            }
        //        }
        //    }
        //}


        //int maxWetness = 0;
        //for (int x = 0; x < mapSize.x; x++)
        //{
        //    for (int y = 0; y < mapSize.y; y++)
        //    {
        //        if (wetMap[x, y] > maxWetness)
        //        {
        //            maxWetness = wetMap[x, y];
        //        }
        //    }
        //}

        int numLayers = pS.applyLayer.Length;
        float[,,] splatMap = new float[mapSize.x, mapSize.y, numLayers];
        float[] splatWeights = new float[numLayers];

        // For each point on the alphamap...
        for (int x = 0; x < mapSize.x; x++)
        {
            for (int y = 0; y < mapSize.y; y++)
            {
                // Get the normalized terrain coordinate that corresponds to the the pointerrain.
                float normX = (float)x / (mapSize.x - 1);
                float normY = (float)y / (mapSize.y - 1);

                //wrong parameter order LOL, took me ages to find the "bug"
                float height = tData.GetHeight(y, x);
                float steepness = tData.GetSteepness(normY, normX);
                Vector3 normal = tData.GetInterpolatedNormal(normY, normX);

                //float random = Random.value;
                //float layerGap = layerSmoothingAngle / 2f;
                float frac = steepness / 90f;

                //float curWetness = wetMap[x, y] / (float)maxWetness;

                for (int i = 0; i < numLayers; i++)
                {
                    if (pS.applyLayer[i])
                    {
                        //for (int j = 0; j < 3; j++)
                        //{

                        //}

                        if (pS.layerExponentValues[i].x >= 0f)
                        {
                            splatWeights[i] = Mathf.Pow(frac, pS.layerExponentValues[i].x);
                        }
                        else
                        {
                            splatWeights[i] = 1f - Mathf.Pow(frac, Mathf.Abs(pS.layerExponentValues[i].x));
                        }

                        //if (pS.layerExponentValues[i].y > 0f)
                        //{
                        //    splatWeights[i] = curWetness;
                        //}
                        //else
                        //{
                        //    splatWeights[i] = 1f - curWetness;
                        //}

                        if (pS.layerNoise)
                        {
                            splatWeights[i] *= UnityEngine.Random.value;
                        }
                    }
                }

                // Steepness is given as an angle, 0..90 degrees. Divide by 90 to get an alpha blending value in the range 0..1.
                //    frac = Mathf.Pow(steepness / 90f, 0.5f);
                //splatWeights[4] = Random.value * frac;
                //splatWeights[0] = Random.value * (1f - frac);

                //if (steepness > maxGreenlandAngle)
                //{
                //    frac = (steepness - maxGreenlandAngle - layerGap) / 90f;
                //    splatWeights[3] += random + (1f - frac);
                //    splatWeights[4] += (1f - random) + frac;
                //}

                //if (steepness <= maxGreenlandAngle + layerGap)
                //{
                //    frac = steepness / (maxGreenlandAngle + layerGap);
                //    splatWeights[0] += random + (1f - frac);
                //    splatWeights[1] += (1f - random) + frac;
                //}

                // normalize layer weights
                for (int i = 0; i < splatWeights.Length; i++)
                {
                    splatMap[x, y, i] = splatWeights[i] /= splatWeights.Sum();
                }
            }
        }
        return splatMap;
    }
}
