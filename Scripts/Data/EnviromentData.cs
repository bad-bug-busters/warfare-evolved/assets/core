using System;
using Unity.Mathematics;
using UnityEngine;

[Serializable]
public struct SimTime
{
    [Range(0, 1)] public float day;
    [Range(0, 1)] public float year;
}

[Serializable]
public struct Planet
{
    [Range(-90, 90)] public float latitude;
    [Range(-180, 180)] public float longitude;
    [Range(-45, 45)] public float axialTilt;
}

[Serializable]
public struct CelestialBody
{
    public Transform transform;
    public Quaternion rotationOffset;
    public Texture2D[] cookies;
}

[Serializable]
public struct Wind
{
    [Range(0, 360)] public float orientation;
    [Range(0, 120)] public float speed;
    [Range(0, 2)] public float turbolence;
    public Texture2D maskTexture;
    [Range(1, 1024)] public float maskScale;
    public float2 maskOffset;
}

public static class EnviromentData
{
    public static SimTime time;
    public static Planet planet;
    public static Wind wind;
    public static float sunPos;
}
