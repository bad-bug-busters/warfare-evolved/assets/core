using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AudioData
{
    public const float maxDB = 140;
    public const float extraDB = 60;
    public const float capDB = maxDB - extraDB;
    public const float speedOfSound = 343;

    public static AudioManager audioManager;
    public static Transform listenerTrans;

    public static AudioSourceData[] audioSourceDatas = new AudioSourceData[256];
}
