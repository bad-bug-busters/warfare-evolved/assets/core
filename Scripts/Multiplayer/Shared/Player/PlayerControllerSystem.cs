using Unity.Entities;
using Unity.NetCode;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;
using Unity.Collections;
using UnityEditor;

//[DisableAutoCreation]
[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
public partial class PlayerControllerSystem : SystemBase
{
    NetworkTick tickLast;

    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;
        var deltaTime = SystemAPI.Time.DeltaTime;
        var world = SystemAPI.GetSingleton<PhysicsWorldSingleton>().CollisionWorld;

        var inputPlayerNonVRData = GetBufferLookup<InputPlayerNonVR>(true);
        var inputPlayerVRHeadData = GetBufferLookup<InputPlayerVRHead>(true);
        var inputPlayerVRHeadHandsData = GetBufferLookup<InputPlayerVRHeadHands>(true);

        var rotSpeed = 4f / sbyte.MaxValue;

        Entities
            .WithReadOnly(world)
            .WithReadOnly(inputPlayerNonVRData)
            .WithReadOnly(inputPlayerVRHeadData)
            .WithReadOnly(inputPlayerVRHeadHandsData)
            .WithAny<InputPlayerNonVR, InputPlayerVRHead, InputPlayerVRHeadHands>()
            .ForEach((Entity entity, ref LocalTransform trans, ref LinearVelocity vel, in PhysicsCollider collider, in PredictedGhostComponent prediction) =>
        {
            var rotate = true;
            var offset = float3.zero;
            var heading = quaternion.identity;
            InputGeneral inputGeneral, inputGeneralLast;

            if (inputPlayerNonVRData.HasBuffer(entity))
            {
                var buffer = inputPlayerNonVRData[entity];
                buffer.GetDataAtTick(tick, out InputPlayerNonVR input);
                buffer.GetDataAtTick(tick , out InputPlayerNonVR inputLast);
                inputGeneral = input.general;
                inputGeneralLast = inputLast.general;
                if (input.noVRHead.freeLook)
                    rotate = false;
            }
            else
            {
                InputVRHead inputVRHead, inputVRHeadLast;
                if (inputPlayerVRHeadData.HasBuffer(entity))
                {
                    var buffer = inputPlayerVRHeadData[entity];
                    buffer.GetDataAtTick(tick, out InputPlayerVRHead input);
                    buffer.GetDataAtTick(tickLast != null ? tickLast : tick, out InputPlayerVRHead inputLast);
                    inputGeneral = input.general;
                    inputGeneralLast = inputLast.general;
                    inputVRHead = input.VRHead;
                    inputVRHeadLast = inputLast.VRHead;
                }
                else
                {
                    var buffer = inputPlayerVRHeadHandsData[entity];
                    buffer.GetDataAtTick(tick, out InputPlayerVRHeadHands input);
                    buffer.GetDataAtTick(tickLast != null ? tickLast : tick, out InputPlayerVRHeadHands inputLast);
                    inputGeneral = input.general;
                    inputGeneralLast = inputLast.general;
                    inputVRHead = input.VRHead;
                    inputVRHeadLast = inputLast.VRHead;
                }
                offset = inputVRHead.pos - inputVRHeadLast.pos;
                offset.y = 0;

                var calcE = MathNew.ToEulerAngles(inputVRHead.rot);
                var finalDir = math.rotate(math.mul(inputVRHead.rot, quaternion.RotateX(math.radians(-calcE.x))), new float3(0, 0, 1));
                finalDir.y = 0;
                heading = quaternion.LookRotationSafe(finalDir, math.up());
            }

            if (rotate && inputGeneral.rotY != 0)
                trans.Rotation = math.mul(quaternion.AxisAngle(math.up(), inputGeneral.rotY * rotSpeed * deltaTime), trans.Rotation);

            //ground check
            var grounded = false;
            var gNormal = float3.zero;
            var gHits = new NativeList<DistanceHit>(Allocator.Temp);
            if (world.OverlapSphere(trans.Position + math.up() * 0.23f, 0.24f, ref gHits, collider.Value.Value.GetCollisionFilter()))
            {
                foreach (var gHit in gHits)
                {
                    if (gHit.Entity.Index != entity.Index)//exclude own collider
                    {
                        grounded = true;
                        gNormal += gHit.SurfaceNormal;
                    }
                }
            }
            //gHits.Dispose();

            //movement
            var moveVel = float3.zero;
            if (grounded)
            {
                if (inputGeneral.moveX != 0 || inputGeneral.moveZ != 0)
                {
                    var move = new float2(inputGeneral.moveX, inputGeneral.moveZ) / sbyte.MaxValue;
                    if (math.abs(move.x) + math.abs(move.y) > 1)
                    {
                        move /= math.length(move);
                    }
                    moveVel = math.rotate(math.mul(trans.Rotation, heading), new float3(move.x, 0, move.y));
                    moveVel = MathNew.ProjectOnPlane(moveVel, math.normalize(gNormal));
                    moveVel *= inputGeneral.sprint ? 8f : 4f;
                    //impulse *= 1f / (math.length(vel.Value) + 1);
                    //moveVel *= deltaTime;
                }
                if (inputGeneral.jump && !inputGeneralLast.jump)
                {
                    moveVel.y += 5;
                }
                moveVel -= vel.Value;
            }
            else
            {
                vel.Value.y -= 9.81f * deltaTime;//gravity
            }

            var newVel = vel.Value + moveVel;
            var airDrag = -0.002f * math.normalizesafe(newVel) * math.lengthsq(newVel);

            vel.Value = newVel + airDrag * deltaTime;

            if (offset.x != 0 || offset.y != 0 || offset.z != 0)
            {
                vel.Value += math.rotate(trans.Rotation, offset) / deltaTime;
            }

            var checkPos = trans.Position + vel.Value * deltaTime;

            //collsion handling
            var hits = new NativeList<DistanceHit>(Allocator.Temp);
            var point1 = checkPos + math.up() * 0.25f;
            var point2 = checkPos + math.up() * 1.5f;
            if (world.OverlapCapsule(point1, point2, 0.25f, ref hits, collider.Value.Value.GetCollisionFilter()))
            {
                var push = float3.zero;
                foreach (var hit in hits)
                {
                    if (hit.Entity.Index != entity.Index)//exclude own collider
                    {
                        //Debug.Log((hits.Length - 1) + " " + hit.Entity.Index + " " + hit.Distance + " " + hit.SurfaceNormal);
                        push += hit.SurfaceNormal * hit.Distance;
                    }
                }
                vel.Value -= push /*/ math.max(hits.Length - 1, 1)*/ / deltaTime;
            }
            //hits.Dispose();
            trans.Position += vel.Value * deltaTime;
        })
            .WithoutBurst().Run();

        tickLast = tick;

        CompleteDependency();
    }
}
