﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


[GhostComponent(SendTypeOptimization = GhostSendType.OnlyInterpolatedClients, OwnerSendType = SendToOwnerType.SendToNonOwner)]
public struct PlayerVRHands : IComponentData
{
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float3x2 pos;
    [GhostField(Quantization = 10000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float4x2 rot;
    [GhostField] public bool2 trigger;
    [GhostField] public bool2 grip;
    public bool2 gripLast;
}
