﻿using Unity.Entities;
using Unity.Mathematics;

public struct InputVRHead : IComponentData
{
    public float3 pos;
    public quaternion rot;
}
