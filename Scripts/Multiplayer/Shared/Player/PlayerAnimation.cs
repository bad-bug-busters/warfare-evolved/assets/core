using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


public struct PlayerAnimation : IComponentData
{
    public Entity rigRoot;

    public Entity hips;
    public Entity spine;
    public Entity chest;
    public Entity upperChest;
    public Entity neck;
    public Entity head;

    public Entity leftShoulder;
    public Entity leftUpperArm;
    public Entity leftLowerArm;
    public Entity leftHand;

    public Entity rightShoulder;
    public Entity rightUpperArm;
    public Entity rightLowerArm;
    public Entity rightHand;

    public Entity leftUpperLeg;
    public Entity leftLowerLeg;
    public Entity leftFoot;
    public Entity leftToes;

    public Entity rightUpperLeg;
    public Entity rightLowerLeg;
    public Entity rightFoot;
    public Entity rightToes;
}
