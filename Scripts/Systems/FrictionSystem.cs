using Unity.Entities;
using Unity.Jobs;
using Unity.Physics;
using Unity.Physics.Extensions;
using Unity.Mathematics;
using Unity.Transforms;

//[DisableAutoCreation]
[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
//[UpdateAfter(typeof(PlayerItemWieldSystem))]
public partial class FrictionSystem : SystemBase
{
    protected override void OnStartRunning()
    {
        var density = 1.204f;
        Entities.ForEach((ref Friction friction) =>
        {
            friction.drag = -0.5f * density * friction.dragCoefficient * friction.crossSection;
        }).Run();
    }

    protected override void OnUpdate()
    {
        var deltaTime = SystemAPI.Time.DeltaTime;

        Entities.ForEach((ref PhysicsVelocity velocity, in WorldTransform trans, in PhysicsMass mass, in Friction friction) =>
        {
            //apply spin
            if (friction.dragSpin.x != 0 || friction.dragSpin.y != 0 || friction.dragSpin.z != 0)
                velocity.ApplyAngularImpulse(mass, friction.dragSpin * math.length(velocity.Linear) * deltaTime);

            var dragFactor = friction.drag * deltaTime;
            var linearDrag = math.normalizesafe(velocity.Linear) * math.lengthsq(velocity.Linear) * dragFactor;
            var angularDrag = math.normalizesafe(velocity.Angular) * math.lengthsq(velocity.Angular) * dragFactor;
            var point = trans.Position + math.rotate(trans.Rotation, friction.centerOfPressure);

            velocity.ApplyImpulse(mass, trans.Position, trans.Rotation, linearDrag, point);
            velocity.ApplyAngularImpulse(mass, angularDrag);
        }).ScheduleParallel();
    }
}
