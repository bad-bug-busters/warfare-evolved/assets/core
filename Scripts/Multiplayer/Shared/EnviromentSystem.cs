using Unity.Entities;
using UnityEngine;

[DisableAutoCreation]
public partial class EnviromentSystem : SystemBase
{
    int secPerDay = 86400;
    protected override void OnCreate()
    {
        RequireForUpdate<DayTime>();
        RequireForUpdate<Weather>();
    }

    protected override void OnUpdate()
    {
        var dayTime = SystemAPI.GetSingleton<DayTime>();
        var weather = SystemAPI.GetSingleton<Weather>();

        dayTime.value += SystemAPI.Time.DeltaTime * dayTime.scale;
        if (dayTime.value >= secPerDay)
        {
            dayTime.value -= secPerDay;
        }
        EnviromentData.time.day = dayTime.value;

         SystemAPI.SetSingleton(dayTime);
         SystemAPI.SetSingleton(weather);
    }
}
