﻿using UnityEngine;

public class SetVR : MonoBehaviour
{
    public bool apply;
    public SettingsDataVR data;
    
    void OnValidate()
    {
        if (apply)
        {
            apply = false;
            SettingsVR.Data = data;
        }
    }
}
