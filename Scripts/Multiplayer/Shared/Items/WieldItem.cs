﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


public struct WieldItem : IComponentData
{
    [GhostField] public int2 ghost;
    public Entity leftEntity;
    public Entity rightEntity;
}
