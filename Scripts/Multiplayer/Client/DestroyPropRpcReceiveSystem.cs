//using Unity.Entities;
//using Unity.NetCode;

//[DisableAutoCreation]
//[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
//[UpdateInGroup(typeof(RpcReceiveSystemGroup))]
//[UpdateAfter(typeof(ReplacePropRpcReceiveSystem))]
//public partial class DestroyPropRpcReceiveSystem : SystemBase
//{
//    protected override void OnUpdate()
//    {
//        var sceneProps = EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>());
//        Entities.ForEach((Entity reqEnt, ref DestroyPropRpc req, ref ReceiveRpcCommandRequestComponent reqSrc) =>
//        {
//            UnityEngine.Debug.Log("receive " + SystemAPI.Time.ElapsedTime);
//            //UnityEngine.Debug.Log("Destroy received rpc entity:" + reqEnt + " prop:" + req.id + " source:" + reqSrc.SourceConnection);
//            var newEvent = EntityManager.CreateEntity();
//            EntityManager.AddComponentData(newEvent, new DestroyPropEvent { id = req.id, isServer = false });
//            EntityManager.DestroyEntity(reqEnt);
//        }).WithStructuralChanges().Run();
//    }
//}
