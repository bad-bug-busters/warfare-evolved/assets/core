﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


[GhostComponent(SendTypeOptimization = GhostSendType.OnlyInterpolatedClients, OwnerSendType = SendToOwnerType.SendToNonOwner)]
public struct PlayerVRHead : IComponentData
{
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float3 pos;
    [GhostField(Quantization = 10000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public quaternion rot;
}
