﻿using Unity.Entities;
using Unity.NetCode;


public struct Arrow : IComponentData
{
    public Entity bow;
    [GhostField] public bool atString;
}
