﻿using Unity.Mathematics;
using UnityEngine;
using Random = Unity.Mathematics.Random;

public class MaterialVariation : MonoBehaviour
{
    public Vector2 tiling = Vector2.one;
    public Vector2 offset;
    public bool randomOffset = true;

    MaterialPropertyBlock mpb;

    void OnValidate()
    {
        var newOffset = offset;
        if (randomOffset)
        {
            var rand = new Random((uint)gameObject.GetInstanceID() + 1);
            newOffset = rand.NextFloat2();
        }

        var tilingOffset = new float4(tiling, newOffset);

        if (mpb == null)
        {
            mpb = new MaterialPropertyBlock();
        }
        mpb.SetVector("_BaseColorMap_ST", tilingOffset);

        foreach (Transform child in transform)
        {
            if (child.TryGetComponent(out Renderer renderer))
            {
                renderer.SetPropertyBlock(mpb);
            }

            if (child.TryGetComponent(out TilingOffsetAuthoring data))
            {
                data.tilingOffset.Value = tilingOffset;
            }
        }
    }
}
