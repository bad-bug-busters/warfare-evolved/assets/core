﻿//using Unity.Entities;
//using Unity.Mathematics;
//using Unity.NetCode;
//using Unity.Transforms;
//using UnityEngine;

//public struct ReplacePropEvent : IComponentData
//{
//    public ushort id;
//    public bool isServer;
//    public float3 hitPos;
//}

//[DisableAutoCreation]
//[UpdateInGroup(typeof(EventSystemGroup))]
//public class ReplacePropEventSystem : ComponentSystem
//{
//    protected override void OnUpdate()
//    {
//        Entities.ForEach((Entity e, ref ReplacePropEvent data) =>
//        {
//            var length = EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>()).Length;
//            var prop = EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>())[data.id].Value;
//            if (prop != Entity.Null)
//            {
//                var hitEnt = EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>())[data.id].Value;

//                var prefab = EntityManager.GetComponentData<FracPrefab>(hitEnt).Value;
//                var curProp = EntityManager.GetComponentData<SceneProp>(hitEnt);
//                var pos = EntityManager.GetComponentData<Translation>(hitEnt);
//                var rot = EntityManager.GetComponentData<Rotation>(hitEnt);
//                EntityManager.DestroyEntity(hitEnt);

//                var newEnt = EntityManager.Instantiate(prefab);
//                EntityManager.SetComponentData(newEnt, curProp);
//                EntityManager.SetComponentData(newEnt, pos);
//                EntityManager.SetComponentData(newEnt, rot);

//                //EntityManager.GetBuffer<ScenePropCollection>(spc)[data.id].Value = newEnt;

//                var closestDist = float.MaxValue;
//                var closestEnt = Entity.Null;

//                var linked = EntityManager.GetBuffer<LinkedEntityGroup>(newEnt);
//                for (int i = 1; i < linked.Length; i++)
//                {
//                    var curPos = EntityManager.GetComponentData<Translation>(linked[i].Value);
//                    var curRot = EntityManager.GetComponentData<Rotation>(linked[i].Value);

//                    curPos.Value = pos.Value + math.rotate(rot.Value, curPos.Value);
//                    curRot.Value = math.mul(rot.Value, curRot.Value);

//                    EntityManager.SetComponentData(linked[i].Value, curPos);
//                    EntityManager.SetComponentData(linked[i].Value, curRot);
//                    EntityManager.SetComponentData(linked[i].Value, new SceneProp { id = (ushort)(length + i - 1)});

//                    EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>()).Add(linked[i].Value);

//                    if (data.isServer)
//                    {
//                        var dist = math.distancesq(curPos.Value, data.hitPos);
//                        if (dist < closestDist)
//                        {
//                            closestDist = dist;
//                            closestEnt = linked[i].Value;
//                        }
//                    }
//                }

//                if (data.isServer)
//                {
//                    var id = EntityManager.GetComponentData<SceneProp>(closestEnt).id;
//                    if (DestructionSystem.HandleHit(closestEnt, id, EntityManager))
//                    {
//                        //broadcast
//                        Entities.WithAll<NetworkStreamInGame>().ForEach((Entity ent, ref NetworkIdComponent netId) =>
//                        {
//                            var reqEnt = EntityManager.CreateEntity();
//                            EntityManager.AddComponentData(reqEnt, new DestroyPropRpc { id = id });
//                            EntityManager.AddComponentData(reqEnt, new SendRpcCommandRequestComponent { TargetConnection = ent });
//                        });
//                    }
//                }
//            }
//            EntityManager.DestroyEntity(e);
//        });
//    }
//}
