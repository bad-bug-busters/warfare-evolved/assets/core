//using Unity.Entities;
//using Unity.NetCode;

//[DisableAutoCreation]
//[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
//[UpdateInGroup(typeof(RpcReceiveSystemGroup))]
//public partial class ReplacePropRpcReceiveSystem : SystemBase
//{
//    protected override void OnUpdate()
//    {
//        Entities.ForEach((Entity reqEnt, ref ReplacePropRpc req, ref ReceiveRpcCommandRequestComponent reqSrc) =>
//        {
//            //UnityEngine.Debug.Log("received rpc entity:" + reqEnt + " prop:" + req.id + " source:" + reqSrc.SourceConnection);
//            var newEvent = EntityManager.CreateEntity();
//            EntityManager.AddComponentData(newEvent, new ReplacePropEvent { id = req.id, isServer = false });
//            EntityManager.DestroyEntity(reqEnt);
//        }).WithStructuralChanges().Run();
//    }
//}
