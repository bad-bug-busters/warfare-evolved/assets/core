﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


public struct ItemHandOrientation : IComponentData
{
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float2 offset;
    [GhostField] public bool leftFlip;
    [GhostField] public bool rightFlip;
    [GhostField] public bool leftInvert;
    [GhostField] public bool rightInvert;
}
