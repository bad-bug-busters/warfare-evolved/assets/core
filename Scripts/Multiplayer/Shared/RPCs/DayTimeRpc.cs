﻿//using Unity.Burst;
using Unity.Entities;
//using Unity.NetCode;
//using Unity.Networking.Transport;

//[BurstCompile]
public struct DayTimeRpc : IComponentData/*, IRpcCommandSerializer<DayTimeRpc>*/
{
    public DayTime dayTime;

    //public PortableFunctionPointer<RpcExecutor.ExecuteDelegate> CompileExecute()
    //{
    //    return new PortableFunctionPointer<RpcExecutor.ExecuteDelegate>(InvokeExecute);
    //}

    //public void Serialize(ref DataStreamWriter writer, in DayTimeRpc data)
    //{
    //    writer.WriteUShort((ushort)(data.dayTime.seconds / 86400f * ushort.MaxValue));
    //    writer.WriteByte((byte)(data.dayTime.scale - 1));
    //}

    //public void Deserialize(ref DataStreamReader reader, ref DayTimeRpc data)
    //{
    //    data.dayTime.seconds = reader.ReadUShort() / (float)ushort.MaxValue * 86400f;
    //    data.dayTime.scale = reader.ReadByte() + 1;
    //}

    //[BurstCompile]
    //private static void InvokeExecute(ref RpcExecutor.Parameters parameters)
    //{
    //    RpcExecutor.ExecuteCreateRequestComponent<DayTimeRpc, DayTimeRpc>(ref parameters);
    //}
}

//public class DayTimeRpcSystem : RpcCommandRequestSystem<DayTimeRpc, DayTimeRpc>
//{
//}
