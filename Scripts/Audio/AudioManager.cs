using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;

[ExecuteAlways]
public class AudioManager : MonoBehaviour
{
    public bool cullSound = true;
    public bool cullAttenuation = true;

    public bool setUp;

    Random random;

    //void OnEnable()
    //{
    //    setUp = true;
    //}

    void OnValidate()
    {
        random = new Random((uint)Time.time + 1);

        if (!cullSound || !cullAttenuation)
        {
            foreach (var asd in AudioData.audioSourceDatas)
            {
                if (!cullSound)
                {
                    asd.isCulled = false;
                    if (asd.repeat.enable && !asd.source.isPlaying)
                        asd.source.Play();
                }

                if (!cullAttenuation && asd.source.volume != asd.volume)
                    asd.source.volume = asd.volume;
            }
        }
    }

    void Start()
    {
        setUp = true;
    }

    void Update()
    {
        if (setUp)
        {
            setUp = false;
            SetUp();
        }

        for (int i = 0; i < AudioData.audioSourceDatas.Length; i++)
        {
            var asd = AudioData.audioSourceDatas[i];

            if (asd != null && asd.source.isActiveAndEnabled)
            {
                var playSound = false;
                var distSq = math.distancesq(asd.trans.position, AudioData.listenerTrans.position);

                for (var j = 0; j < asd.instances.Length; j++)
                {
                    if (asd.instances[j].isPropagating)
                    {
                        if (asd.instances[j].propDist < asd.source.maxDistance || !cullSound)
                        {
                            asd.instances[j].propDist += AudioData.speedOfSound * Time.deltaTime;
                            if (asd.instances[j].propDist * asd.instances[j].propDist >= distSq)
                            {
                                //Debug.Log("instance: " + j + " dist: " + asd.instances[j].propDist + " seconds: " + asd.instances[j].propDist / AudioData.speedOfSound);
                                asd.instances[j].isPropagating = false;
                                asd.instances[j].propDist = 0;
                                playSound = true;
                                break;//only fetch the first sound to play
                            }
                        }
                        else if (asd.instances[j].propDist > asd.source.maxDistance && cullSound)
                        {
                            asd.instances[j].isPropagating = false;
                            asd.instances[j].propDist = 0;
                        }
                    }
                }

                if (cullSound)// do audible check, cull sounds
                {
                    if (distSq > asd.maxDistSq)
                    {
                        asd.isCulled = true;
                        playSound = false;
                        if (asd.source.isPlaying)
                        {
                            asd.source.Pause();
                        }
                    }
                    else
                    {
                        asd.isCulled = false;
                        if (asd.source.loop)
                        {
                            playSound = true;
                        }
                    }
                }

                if (playSound)
                {
                    if (!asd.source.loop)
                    {
                        random.InitState((uint)(1 + gameObject.GetInstanceID() + asd.emitTime * 1000));
                        
                        if (asd.audioSourceSO != null)
                            asd.source.clip = asd.audioSourceSO.audioClips[random.NextInt(asd.audioSourceSO.audioClips.Length)];

                        if (!asd.randomPerSource && asd.randomPitchMinMax.x != asd.randomPitchMinMax.y)
                            asd.source.pitch = random.NextFloat(asd.randomPitchMinMax.x, asd.randomPitchMinMax.y);
                    }
                    asd.source.Play();
                }

                if (cullAttenuation && !asd.isCulled && distSq <= asd.maxDistSq)
                    asd.source.volume = asd.volume * math.max(1 - math.max(distSq - asd.fullVolDistSq, 0) / asd.maxDistSqMinusfullVolDistSq, 0);


                var emitSound = false;
                if (asd.emitTime != 0 && Time.time >= asd.emitTime)
                {
                    if (asd.repeat.enable)
                    {
                        var intervalDiv = 1f;
                        if (asd.repeat.sunPosRepeat)//sun altitude related repeat
                        {
                            intervalDiv = asd.repeat.sunPosCurve.Evaluate(EnviromentData.sunPos);
                            //Debug.Log("curve check");
                        }
                        asd.emitTime = Time.time + random.NextFloat(asd.repeat.intervalMinMax.x, asd.repeat.intervalMinMax.y) / math.max(intervalDiv, 0.1f);

                        if (intervalDiv != 0)
                        {
                            emitSound = true;
                            //Debug.Log(Time.time + "...emit " + intervalDiv + " next emit time: " + asd.emitTime);
                        }
                    }
                    else
                    {
                        emitSound = true;
                    }
                }

                if (emitSound)
                {
                    for (var j = 0; j < asd.instances.Length; j++)
                    {
                        if (!asd.instances[j].isPropagating)
                        {
                            asd.instances[j].isPropagating = true;
                            asd.instances[j].propDist = asd.emitRadius;
                            //Debug.Log("emit instance: " + j + " num instances: " + asd.instances.Length);
                            break;
                        }
                    }
                }
            }
        }
    }

    void SetUp()
    {
        AudioData.audioManager = GetComponent<AudioManager>();
        AudioData.listenerTrans = FindObjectOfType<AudioListener>().transform;

        var audioSources = FindObjectsOfType<AudioSourceSetup>();

        for (var i = 0; i < AudioData.audioSourceDatas.Length; i++)
        {
            AudioData.audioSourceDatas[i] = i < audioSources.Length ? audioSources[i].data : null;
        }
    }
}
