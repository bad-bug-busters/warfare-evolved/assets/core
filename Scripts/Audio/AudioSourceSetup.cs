﻿using System;
using UnityEngine;
using Unity.Mathematics;
using Random = Unity.Mathematics.Random;
using UnityEditor;

[Serializable]
public struct AudioInstance
{
    public bool isPropagating;
    public float propDist;
}

[Serializable]
public struct AudioRepeat
{
    public bool enable;
    public float2 intervalMinMax;
    public bool sunPosRepeat;
    public AnimationCurve sunPosCurve;
}

[Serializable]
public class AudioSourceData
{
    public AudioSourceSO audioSourceSO;
    [Range(20, AudioData.maxDB)] public float volumeDB = 80;
    public AudioRepeat repeat = new AudioRepeat { intervalMinMax = new float2(10, 10) };
    public float2 randomPitchMinMax = new float2(0.875f, 1.125f);
    public bool randomPerSource;

    [HideInInspector] public float emitTime;
    [HideInInspector] public AudioInstance[] instances;
    [HideInInspector] public bool isCulled;
    [HideInInspector] public float volume;
    [HideInInspector] public Transform trans;
    [HideInInspector] public AudioSource source;
    [HideInInspector] public float emitRadius;
    [HideInInspector] public float fullVolDist;
    [HideInInspector] public float fullVolDistSq;
    [HideInInspector] public float maxDistSq;
    [HideInInspector] public float maxDistSqMinusfullVolDistSq;
}

//[ExecuteAlways]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(AudioLowPassFilter))]
[RequireComponent(typeof(ResonanceAudioSource))]
public class AudioSourceSetup : MonoBehaviour
{
    const float minFreq = 10;
    const float maxFreq = 22000;
    const float maxSubMinFreq = maxFreq - minFreq;

    public AudioSourceData data;
    
    [Range(0.001f, 1000)] public float emitRadius = 0.001f;
    [Range(minFreq, maxFreq)] public float preLowPass = maxFreq;

    public bool3 curveSetup = new bool3(true);

    //public bool setUp;

    AudioLowPassFilter lowPass;
    ResonanceAudioSource resonance;

    Random random;

    //float minVolume = 0.0005f;//anything below wont start playing, bug?

    //void OnEnable()
    //{
    //    Debug.Log("enable");
    //    setUp = true;
    //}

    //void OnDisable()
    //{
    //    Debug.Log("disable");
    //    if (AudioData.audioManager.audioSourceDatas.Contains(data))
    //        AudioData.audioManager.audioSourceDatas.Remove(data);
    //}

    void OnValidate()
    {
        random = new Random((uint)Time.time + 1);
        data.emitTime = Time.time;
        if (data.repeat.enable)
        {
            data.emitTime += random.NextFloat(data.repeat.intervalMinMax.y);
        }

        data.trans = transform;
        data.source = GetComponent<AudioSource>();
        lowPass = GetComponent<AudioLowPassFilter>();
        resonance = GetComponent<ResonanceAudioSource>();

        data.emitRadius = emitRadius;
        data.fullVolDist = emitRadius + 1;
        data.fullVolDistSq = data.fullVolDist * data.fullVolDist;
        data.volume = math.pow(10, (math.min(data.volumeDB, AudioData.capDB) - AudioData.capDB) / 20);//convert to linear volume
        resonance.gainDb = math.max(data.volumeDB - AudioData.capDB, 0);

        if (curveSetup.x)
            data.source.rolloffMode = AudioRolloffMode.Custom;

        if (data.randomPerSource)
            data.source.pitch = random.NextFloat(data.randomPitchMinMax.x, data.randomPitchMinMax.y);

        data.source.volume = data.volume;
        data.source.minDistance = data.fullVolDist;

        var maxDist = math.pow(10, data.volumeDB / 20);//distance at which sound level reaches threshold
        maxDist *= 1 + emitRadius;//scales by emission area
        maxDist = math.sqrt(maxDist);//approximate occlusion by scene geometry

        data.source.maxDistance = data.fullVolDist - 1 + maxDist;
        data.maxDistSq = data.source.maxDistance * data.source.maxDistance;
        data.maxDistSqMinusfullVolDistSq = data.maxDistSq - data.fullVolDistSq;

        var numInstances = (data.source.maxDistance - emitRadius) / (AudioData.speedOfSound * data.repeat.intervalMinMax.x) + 1;//calculate how much sounds can be played while the first sound is still traveling
        data.instances = new AudioInstance[(uint)numInstances];

        var key = new Keyframe { weightedMode = WeightedMode.Both };
        var distAdder = data.emitRadius + CutOffDistance(maxFreq);
        var numCurves = distAdder < data.source.maxDistance ? 3 : 2;
        for (int i = 0; i < numCurves; i++)
        {
            if (curveSetup[i])
            {
                var curve = new AnimationCurve();

                var curDist = i == 2 ? distAdder / data.source.maxDistance : 0;
                var curVal = i == 2 ? maxFreq : (i == 0 ? 1f : 0.5f);

                var iKey = 0;
                while (curDist <= 1)
                {
                    if (iKey == 1 && i != 2)
                        curDist = (i == 0 ? data.fullVolDist : emitRadius) / data.source.maxDistance;

                    key.time = curDist;
                    key.value = GetKeyValue(i, curVal);

                    var keySkipped = false;
                    if (curDist < 0.75f)// not too close to max dist, could cause weird tangents to second last key
                        curve.AddKey(key);
                    else
                        keySkipped = true;

                    curVal /= 2;
                    curDist = i == 2 ? (emitRadius + CutOffDistance(curVal)) / data.source.maxDistance : curDist * 2;

                    if (curDist > 1)// next key would be out of range, add extra key
                    {
                        key.time = 1;
                        key.value = iKey == 0 && keySkipped ? 1 : math.min(GetKeyValue(i, curVal) * curDist, key.value);// calculate value at max dist
                        curve.AddKey(key);
                    }
                    iKey++;
                }

                for (int j = 1; j < curve.length - 1; j++)
                {
                    curve.SmoothTangents(j, -0.5f);
                }

                if (i == 2)
                    lowPass.customCutoffCurve = curve;
                else
                    data.source.SetCustomCurve(i == 0 ? AudioSourceCurveType.CustomRolloff : AudioSourceCurveType.Spread, curve);
            }
        }
    }

    //void Update()
    //{
    //    if (setUp)
    //    {
    //        setUp = false;
    //        SetUp();
    //    }
    //}

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, emitRadius);

        if (data.source.isPlaying)
            Gizmos.DrawSphere(transform.position, data.fullVolDist);

        Gizmos.color = Color.cyan;
        for (int i = 0; i < data.instances.Length; i++)
        {
            if (data.instances[i].isPropagating)
            {
                Gizmos.DrawWireSphere(transform.position, data.instances[i].propDist);
            }
        }
    }

    //void SetUp()
    //{
    //    if (!AudioData.audioManager.audioSourceDatas.Contains(data))
    //        AudioData.audioManager.audioSourceDatas.Add(data);
    //}

    float GetKeyValue(int i, float curVal)
    {
        return i == 2 ? (math.clamp(curVal, minFreq, preLowPass) - minFreq) / maxSubMinFreq : curVal;
    }

    float CutOffDistance(float frequency)
    {
        return 3.0103f / Absorption(frequency);
    }

    float Absorption(float frequency)
    {
        //In dB/m

        //   frequency in Hz
        //   temperature in °C
        //   relative humidity in %
        //   atmospheric pressure in Pa

        //   From http://en.wikibooks.org/wiki/Engineering_Acoustics/Outdoor_Sound_Propagation
        //   See __main__ for actual curves.

        // 1 atm in Pa
        var ps0 = 1.01325e5f;

        var temperature = 20;
        var humidity = 75;
        var pressure = ps0;

        var t = temperature + 273.15f;
        var t0 = 293.15f;
        var t01 = 273.16f;

        var cSat = -6.8346f * math.pow(t01 / t, 1.261f) + 4.6151f;
        var rhoSat = math.pow(10, cSat);
        var h = rhoSat * humidity * ps0 / pressure;

        var psDivPs0 = pressure / ps0;
        var t0DivT = t0 / t;
        var frn = psDivPs0 * math.pow(t0DivT, 0.5f) * (9 + 280 * h * math.exp(-4.17f * (math.pow(t0DivT, 1f / 3f) - 1)));
        var fro = psDivPs0 * (24.0f + 4.04e4f * h * (0.02f + h) / (0.391f + h));

        var f2 = frequency * frequency;
        var alpha = f2 * (
            1.84e-11f / (math.pow(t0DivT, 0.5f) * psDivPs0)
            + math.pow(t / t0, -2.5f)
            * (
                0.10680f * math.exp(-3352 / t) * frn / (f2 + frn * frn)
            + 0.01278f * math.exp(-2239.1f / t) * fro / (f2 + fro * fro)
            )
            );

        return 20 * alpha / math.log(10);
    }
}
