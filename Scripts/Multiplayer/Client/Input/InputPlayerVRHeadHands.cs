﻿using Unity.NetCode;

public struct InputPlayerVRHeadHands : ICommandData
{
    public NetworkTick Tick {get; set;}
    public InputGeneral general;
    public InputVRHead VRHead;
    public InputVRHands VRHands;
}
