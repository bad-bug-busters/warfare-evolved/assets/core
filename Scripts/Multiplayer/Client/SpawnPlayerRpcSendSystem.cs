using Unity.Collections;
using Unity.Entities;
using Unity.NetCode;

// When client has a connection with network id, go in game and tell server to also go in game
//[DisableAutoCreation]
[WorldSystemFilter(WorldSystemFilterFlags.ClientSimulation)]
public partial class SpawnPlayerRpcSendSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireForUpdate<PlayerSpawner>();
        RequireForUpdate(GetEntityQuery(ComponentType.ReadOnly<NetworkIdComponent>(), ComponentType.Exclude<NetworkStreamInGame>()));
        RequireForUpdate<SettingsDataVR>();
    }

    protected override void OnUpdate()
    {
        var ecb = new EntityCommandBuffer(Allocator.Temp);

        var vrSettings = SystemAPI.GetSingleton<SettingsDataVR>();
        Entities.WithNone<NetworkStreamInGame>().ForEach((Entity ent, ref NetworkIdComponent id) =>
        {
            ecb.AddComponent<NetworkStreamInGame>(ent);
            var reqEnt = ecb.CreateEntity();

            var rpc = new SpawnPlayerRpc { };
            if (vrSettings.enableVR)
                rpc.inputMode = (byte)(vrSettings.useHandControllers ? 2 : 1);
            else
                rpc.inputMode = 0;

            ecb.AddComponent(reqEnt, rpc);
            ecb.AddComponent(reqEnt, new SendRpcCommandRequestComponent { TargetConnection = ent });
        }).Run();

        ecb.Playback(EntityManager);
    }
}
