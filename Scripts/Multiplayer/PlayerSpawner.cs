using Unity.Entities;


public struct PlayerSpawner : IComponentData
{
    public Entity player;
    public Entity playerHMD;
    public Entity playerVR;
}
