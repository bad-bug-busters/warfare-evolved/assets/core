﻿using Unity.Entities;

public struct InputGeneral : IComponentData
{
    public sbyte moveX;
    public sbyte moveZ;
    public sbyte rotY;
    public byte stance;
    public bool sprint;
    public bool jump;
    public bool kick;
}
