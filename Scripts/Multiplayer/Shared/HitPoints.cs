﻿using Unity.Entities;


public struct HitPoints : IComponentData
{
    public int Value;
}
