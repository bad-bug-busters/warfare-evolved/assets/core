Shader "Hidden/Shader/HeatHaze"
{
    HLSLINCLUDE

    #pragma target 4.5
    #pragma only_renderers d3d11 playstation xboxone xboxseries vulkan metal switch

    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Common.hlsl"
    #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/ShaderLibrary/ShaderVariables.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/FXAA.hlsl"
    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/PostProcessing/Shaders/RTUpscale.hlsl"

    #include "Packages/com.unity.render-pipelines.high-definition/Runtime/Lighting/AtmosphericScattering/AtmosphericScattering.hlsl"

    struct Attributes
    {
        uint vertexID : SV_VertexID;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    struct Varyings
    {
        float4 positionCS : SV_POSITION;
        float2 texcoord   : TEXCOORD0;
        UNITY_VERTEX_OUTPUT_STEREO
    };

    Varyings Vert(Attributes input)
    {
        Varyings output;
        UNITY_SETUP_INSTANCE_ID(input);
        UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
        output.positionCS = GetFullScreenTriangleVertexPosition(input.vertexID);
        output.texcoord = GetFullScreenTriangleTexCoord(input.vertexID);
        return output;
    }

    // List of properties to control your post process effect
    float _Intensity;
    float2 _DistanceHeight;
    float4 _FrequencyScale;
    float _LayerMix;
    TEXTURECUBE(_DistortionCubeMap);
    float4 _DebugMap;
    float _FOV;
    float3 _CamPos;
    
    TEXTURE2D_X(_InputTexture);

    float4 CustomPostProcess(Varyings input) : SV_Target
    {
        UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

        uint2 positionSS = input.texcoord * _ScreenSize.xy;
        float aspectRatio = _ScreenSize.y / _ScreenSize.x;

        float depth = LoadCameraDepth(positionSS);
        //float linearEyeDepth = LinearEyeDepth(depth, _ZBufferParams);
        
        float3 worldPos = ComputeWorldSpacePosition(input.texcoord, depth, _InvViewProjMatrix);
        //float3 worldPosReal = worldPos + _CamPos;
        float3 worldDir = normalize(worldPos);

        float4 mapSample = SAMPLE_TEXTURECUBE(_DistortionCubeMap, s_linear_clamp_sampler, worldDir);

        float2 timeMulti = _TimeParameters.xx * _FrequencyScale.xy;

        float3 viewPos = GetCurrentViewPosition();

        float H = _DistanceHeight.y * 0.144765;
        float distAmount = OpticalDepthHeightFog(1 / _DistanceHeight.x, -_CamPos.y, float2(1 / H, H), worldDir.y, viewPos.y, distance(worldPos, viewPos));
        distAmount = 1 - TransmittanceFromOpticalDepth(distAmount);

        float2 offset1 = sin(timeMulti.x * _FrequencyScale.x + mapSample.xy * _FrequencyScale.z);
        float2 offset2 = cos(timeMulti.y * _FrequencyScale.y + mapSample.zw * _FrequencyScale.w);
        float2 offsetDebug = lerp(offset1, offset2, _LayerMix) / 2 + 0.5;

        float2 offset = (offsetDebug - 0.5) * distAmount * _Intensity / _FOV;
        offset.x *= aspectRatio;

        float2 distUV = clamp(input.texcoord + offset, 0.000001, 0.999999); // 0 and 1 return black pixels

        float depthOff = LoadCameraDepth(distUV);
        if (depthOff > depth)// avoid sampling pixels which are infront of the current sampled pixel
        {
            distUV = input.texcoord;
        }

        //float3 outColor = LOAD_TEXTURE2D_X(_InputTexture, distUV).xyz;

        float3 outColor = SAMPLE_TEXTURE2D_X(_InputTexture, s_linear_clamp_sampler, ClampAndScaleUVForBilinearPostProcessTexture(distUV)).xyz;

        if (_DebugMap.w != 0)
        {
            outColor = lerp(outColor, float3(offsetDebug, distAmount) * _DebugMap.xyz, _DebugMap.w);
        }
        return float4(outColor, 1);

        //float density = 1 - exp(-linearEyeDepth * 0.001);
        //return float4(lerp(outColor, float3(1, 1, 1), density), 1);

    }

    ENDHLSL

    SubShader
    {
        Pass
        {
            Name "HeatHaze"

            ZWrite Off
            ZTest Always
            Blend Off
            Cull Off

            HLSLPROGRAM
                #pragma fragment CustomPostProcess
                #pragma vertex Vert
            ENDHLSL
        }
    }
    Fallback Off
}
