﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowAuthoringEditor : MonoBehaviour
{
    [Range(0, 0.6f)] public float powerStroke = 0;
    [Range(0, 1)] public float braceHeight = 0.16f;
    [Range(0, 1)] public float stringReach = 0.935f;

    MaterialPropertyBlock mpb;

    void OnValidate()
    {
        if (mpb == null)
        {
            mpb = new MaterialPropertyBlock();
        }
        mpb.SetFloat("_PowerStroke", powerStroke);
        mpb.SetFloat("_BraceHeight", braceHeight);
        mpb.SetFloat("_StringReach", stringReach);

        foreach (Transform child in transform)
        {
            if (child.TryGetComponent(out Renderer renderer))
            {
                renderer.SetPropertyBlock(mpb);
            }
        }
    }
}
