using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.XR;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateBefore(typeof(InputGeneralSystem))]
public partial class InputVRHandsSystem : SystemBase
{
    protected override void OnCreate()
    {
        if (SettingsVR.curData.enableVR && SettingsVR.curData.useHandControllers)
        {
            EntityManager.CreateEntity(typeof(InputVRHands));
        }
        RequireForUpdate<InputVRHands>();
    }

    protected override void OnUpdate()
    {
        var device = new NativeArray<InputDevice>(2, Allocator.Temp);
        device[0] = InputDevices.GetDeviceAtXRNode(XRNode.LeftHand);
        device[1] = InputDevices.GetDeviceAtXRNode(XRNode.RightHand);

        if (device[0].isValid && device[1].isValid)
        {
            var input = SystemAPI.GetSingleton<InputVRHands>();

            for (int i = 0; i < 2; i++)
            {
                if (device[i].TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 pos))
                {
                    input.pos[i] = pos;
                }
                if (device[i].TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rot))
                {
                    input.rot[i] = ((quaternion)rot).value;
                }
                input.trigger[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Trigger Left" : "Trigger Right"));
                input.grip[i] = Convert.ToBoolean(Input.GetAxis(i == 0 ? "Grip Left" : "Grip Right"));
            }
            SystemAPI.SetSingleton(input);
        }

        device.Dispose();
    }
}
