﻿using Unity.Entities;
using Unity.NetCode;

[UpdateAfter(typeof(RpcReceiveSystemGroup))]
[WorldSystemFilter(WorldSystemFilterFlags.ServerSimulation | WorldSystemFilterFlags.ClientSimulation)]
public class EventSystemGroup : ComponentSystemGroup
{
}
