using Unity.Entities;
using Unity.NetCode;

//[DisableAutoCreation]
[UpdateInGroup(typeof(PredictedSimulationSystemGroup))]
public partial class PlayerNoVRHandsSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var tick = SystemAPI.GetSingleton<NetworkTime>().ServerTick;

        var inputPlayerNonVRData = GetBufferLookup<InputPlayerNonVR>(true);
        var inputPlayerVRHeadData = GetBufferLookup<InputPlayerVRHead>(true);

        Entities
            .WithReadOnly(inputPlayerNonVRData)
            .WithReadOnly(inputPlayerVRHeadData)
            .WithAny<InputPlayerNonVR, InputPlayerVRHead>()
            .ForEach((Entity entity, ref PlayerNoVRHands noVRHands, in PredictedGhostComponent prediction) =>
        {
            InputNoVRHands inputNoVRHands;
            if (inputPlayerNonVRData.HasBuffer(entity))
            {
                var buffer = inputPlayerNonVRData[entity];
                buffer.GetDataAtTick(tick, out InputPlayerNonVR input);
                inputNoVRHands = input.noVRHands;
            }
            else
            {
                var buffer = inputPlayerVRHeadData[entity];
                buffer.GetDataAtTick(tick, out InputPlayerVRHead input);
                inputNoVRHands = input.noVRHands;
            }
            noVRHands.interact = inputNoVRHands.interact;
            noVRHands.drop = inputNoVRHands.drop;
        })
            .ScheduleParallel();
    }
}
