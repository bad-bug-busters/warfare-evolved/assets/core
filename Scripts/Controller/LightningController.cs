﻿using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[ExecuteInEditMode]
public class LightningController : MonoBehaviour
{
    public VisualEffect vfx;
    public AnimationCurve lightCurve;
    public float duration = 5;
    public float repeatPeriod = 10;
    public Vector3 startPos = new Vector3(0, 1000, 0);
    public Vector3 targetpos;
    public float lightStrength = 10000000;

    Transform lightTransform;
    Light lightComponent;
    HDAdditionalLightData lightData;
    float repeatTime;
    float startTime;
    float endTime;
    float curTime;

    void OnValidate()
    {
        Setup();
    }

    void Start()
    {
        Setup();
    }

    // Update is called once per frame
    void Update()
    {
        curTime = Time.time;

        if (repeatPeriod != 0 && curTime >= repeatTime)
        {
            repeatTime = curTime + repeatPeriod;
            StartLightning();
        }

        if (curTime >= startTime && curTime <= endTime)
        {
            var strength = (curTime - startTime) % 0.1f;
            lightData.intensity = lightStrength * strength;
            vfx.SetFloat("Strength", strength);
        }
        else
        {
            lightComponent.enabled = false;
        }
    }

    void Setup()
    {
        lightTransform = transform.GetChild(0);
        lightComponent = lightTransform.GetComponent<Light>();
        lightData = lightTransform.GetComponent<HDAdditionalLightData>();
    }

    void StartLightning()
    {
        startTime = curTime;
        endTime = startTime + duration;

        vfx.startSeed = (uint)(curTime * 1000000);
        vfx.SetFloat("Duration", duration);
        vfx.Play();

        lightTransform.position = Vector3.Lerp(startPos, targetpos, 0.5f);
        lightComponent.enabled = true;
    }
}
