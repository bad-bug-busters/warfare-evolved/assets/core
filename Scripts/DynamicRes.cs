using UnityEngine;
using UnityEngine.Rendering;

[ExecuteAlways]
public class DynamicRes : MonoBehaviour
{
    [Range(0, 100)] public float resScale = 50;
    //[Range(0, 360)] public int frameLimit = 60;

    public float SetDynamicResolutionScale()
    {
        return resScale;
    }

    void Start()
    {
        DynamicResolutionHandler.SetDynamicResScaler(SetDynamicResolutionScale, DynamicResScalePolicyType.ReturnsPercentage);
    }

    //void Update()
    //{
    //    if (Application.targetFrameRate != frameLimit)
    //    {
    //        Application.targetFrameRate = 30;
    //    }
    //}
}
