using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class ControllerOffset : MonoBehaviour
{
    public Transform[] controllers= new Transform[2];
    public float3 pos;
    public Quaternion rot;
    void OnValidate()
    {
        for (int i = 0; i < controllers.Length; i++)
        {
            controllers[i].localPosition = i == 0 ? pos : pos * new float3(-1, 1, 1);
            controllers[i].localRotation = rot;
        }
    }
}
