﻿using Unity.Entities;
using Unity.Mathematics;


public struct AgentMovement : IComponentData
{
    public float3 vrOffsetFix;
    public float3 axialMove;
    public float heading;
    public bool sprint;
    public bool jump;
}
