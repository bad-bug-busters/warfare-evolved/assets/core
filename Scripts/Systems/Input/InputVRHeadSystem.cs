using Unity.Entities;
using Unity.NetCode;
using UnityEngine;
using UnityEngine.XR;

//[DisableAutoCreation]
[UpdateInGroup(typeof(GhostInputSystemGroup))]
[UpdateBefore(typeof(InputGeneralSystem))]
public partial class InputVRHeadSystem : SystemBase
{
    protected override void OnCreate()
    {
        if (SettingsVR.curData.enableVR)
        {
            EntityManager.CreateEntity(typeof(InputVRHead));
        }
        RequireForUpdate<InputVRHead>();
    }

    protected override void OnUpdate()
    {
        var device = InputDevices.GetDeviceAtXRNode(XRNode.CenterEye);
        if (device.isValid)
        {
            var input = SystemAPI.GetSingleton<InputVRHead>();

            if (device.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 pos))
            {
                input.pos = pos;
            }
            if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rot))
            {
                input.rot = rot;
            }
            SystemAPI.SetSingleton(input);
        }
    }
}
