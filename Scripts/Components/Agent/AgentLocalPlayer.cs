﻿using Unity.Entities;


public struct AgentLocalPlayer : IComponentData
{
    public bool Value;
}