#if UNITY_EDITOR

using Unity.Mathematics;
using UnityEngine;
using Random = Unity.Mathematics.Random;
using UnityEditor;

[ExecuteAlways]
public class ShapeVariation : MonoBehaviour
{
    //public Quaternion rotation;
    //public Vector3 scale = Vector3.one;

    public bool3 randomRot;
    public float3 rotStep;

    public bool3 randomFlip;

    public bool specialRandom;

    public void OnValidate()
    {
        var prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected)
        {
            var rand = new Random((uint)gameObject.GetInstanceID() + 1);
            var rotAdder = new float3();
            var scaleMulti = new float3(1, 1, 1);
            for (int i = 0; i < 3; i++)
            {
                if (randomRot[i])
                {
                    rotAdder[i] = rotStep[i] != 0 ? rand.NextInt(360 / math.max((int)rotStep[i], 1)) * rotStep[i] : rand.NextFloat(360);
                }
                if (randomFlip[i] && rand.NextBool())
                {
                    scaleMulti[i] *= -1;
                }
            }

            if (specialRandom && rand.NextBool())
            {
                rotAdder.x += 180;
                rotAdder.z += 90;
            }

            transform.localRotation *= Quaternion.Euler(rotAdder);
            transform.localScale *= scaleMulti;
        }
    }
}
#endif