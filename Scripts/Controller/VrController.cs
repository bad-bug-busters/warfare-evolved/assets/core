﻿using System;
using UnityEngine;
using UnityEngine.XR;

public class VrController : MonoBehaviour
{
    public bool showHeadset = true;
    public bool showCurrentControllers = true;
    public bool showOtherControllers = false;

    public Vector3[] nodePos = { new Vector3(0f, -0.04f, -0.1f), new Vector3(-0.03f, 0.05f, -0.15f)};
    public Vector3[] nodeRot = { new Vector3(0f, 0f, 0f), new Vector3(0f, -60f, 90f)};

    Transform headOrientation;
    void OnValidate()
    {
        Configurate();
    }

    void Start()
    {
        Configurate();
    }

    void LateUpdate()
    {
        for (int i = 0; i < 3; i++)
        {
            var device = InputDevices.GetDeviceAtXRNode((XRNode)(i + 3));
            if (device.isValid)
            {
                if (device.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 nodePos))
                {
                    transform.GetChild(i).localPosition = nodePos;
                }
                if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion nodeRot))
                {
                    transform.GetChild(i).localRotation = nodeRot;
                }
            }
        }
        //transform.GetChild(3).localRotation = InputSystem.heading;
        //Debug.DrawRay(transform.GetChild(3).position, transform.GetChild(3).TransformDirection(Vector3.forward), Color.blue);
    }

    void Configurate()
    {
        headOrientation = transform.GetChild(0).GetChild(1);

        //for (int i = 0; i < 3; i++)
        //{
        //    InputVRHeadSystem.nodeOffset[i, 0] = nodePos[Mathf.Min(i, 1)];
        //    InputVRHeadSystem.nodeOffset[i, 1] = nodeRot[Mathf.Min(i, 1)] * Mathf.Deg2Rad;
        //}
        //InputVRHeadSystem.nodeOffset[2, 0].x *= -1;
        //InputVRHeadSystem.nodeOffset[2, 1].y *= -1;
        //InputVRHeadSystem.nodeOffset[2, 1].z *= -1;

        //transform.GetChild(0).GetChild(0).gameObject.SetActive(showHeadset);

        //for (int i = 1; i < 3; i++)
        //{
        //    for (int j = 0; j < Enum.GetValues(typeof(HMDdevice)).Length; j++)
        //    {
        //        if (j == InputSystem.hmd)
        //        {
        //            transform.GetChild(i).GetChild(j).gameObject.SetActive(showCurrentControllers);
        //        }
        //        else
        //        {
        //            transform.GetChild(i).GetChild(j).gameObject.SetActive(showOtherControllers);
        //        }
        //    }
        //}
    }

    public void SowControllers(bool value)
    {
        transform.GetChild(1).gameObject.SetActive(value);
        transform.GetChild(2).gameObject.SetActive(value);
    }
}
