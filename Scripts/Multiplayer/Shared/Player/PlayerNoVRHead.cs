﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.NetCode;


//[GhostComponent(SendTypeOptimization = GhostSendType.OnlyInterpolatedClients, OwnerSendType = SendToOwnerType.SendToNonOwner)]
public struct PlayerNoVRHead : IComponentData
{
    [GhostField(Quantization = 1000, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float2 headRot;
    [GhostField(Quantization = 100, Smoothing = SmoothingAction.InterpolateAndExtrapolate)] public float headHeight;
}
