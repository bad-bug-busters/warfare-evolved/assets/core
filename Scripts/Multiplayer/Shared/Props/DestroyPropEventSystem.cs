﻿//using Unity.Entities;
//using Unity.Physics;
//using Unity.Physics.Systems;
//using Unity.Mathematics;
//using Unity.NetCode;
//using Unity.Transforms;
//using UnityEngine;
//using RaycastHit = Unity.Physics.RaycastHit;

//public struct DestroyPropEvent : IComponentData
//{
//    public ushort id;
//    public bool isServer;
//}

//[DisableAutoCreation]
//[UpdateInGroup(typeof(EventSystemGroup))]
//[UpdateAfter(typeof(ReplacePropEventSystem))]
//public class DestroyPropEventSystem : SystemBase
//{
//    BuildPhysicsWorld buildPhysicsWorld;
//    CollisionFilter collisionFilter;

//    float3x4 dir = new float3x4
//    (
//        new float3(-0.5f, 0, 0),
//        new float3(0.5f, 0, 0),
//        new float3(0, 0, -0.5f),
//        new float3(0, 0, 0.5f)
//    );

//    protected override void OnCreate()
//    {
//        buildPhysicsWorld = World.GetOrCreateSystemManaged<BuildPhysicsWorld>();

//        collisionFilter = new CollisionFilter()
//        {
//            BelongsTo = ~0u,
//            CollidesWith = 1u << 0,
//            GroupIndex = 0
//        };
//    }

//    protected override void OnUpdate()
//    {
//        var world = buildPhysicsWorld.PhysicsWorld.CollisionWorld;
//        var filter = collisionFilter;
//        var time = Time.ElapsedTime;

//        Entities.ForEach((Entity e, ref DestroyPropEvent data) =>
//        {
//            var prop = EntityManager.GetBuffer<ScenePropCollection>(SystemAPI.GetSingletonEntity<ScenePropCollection>())[data.id].Value;
//            if (prop != Entity.Null)
//            {
//                var pos = EntityManager.GetComponentData<Translation>(prop).Value;
//                Debug.Log("destroy prop: " + prop + " time: " + time);
//                EntityManager.DestroyEntity(prop);

//                //if (data.isServer)
//                //{
//                //    UnityEngine.Debug.Log("is server");

//                //    for (int i = 0; i < 5; i++)
//                //    {
//                //        var input = CastInput.Ray(pos + (i == 4 ? new float3(0, 0.5f, 0) : dir[i]), pos, filter);
//                //        RaycastHit hit;
//                //        if (world.CastRay(input, out hit))
//                //        {
//                //            var hitEnt = world.Bodies[hit.RigidBodyIndex].Entity;
//                //            UnityEngine.Debug.Log("checkEnt " + hitEnt);
//                //            if (hitEnt != prop && EntityManager.HasBuffer<HitPoints>(hitEnt))
//                //            {
//                //                EntityManager.AddComponentData(hitEnt, new PropCheckSupport { checkTime = time + 1 });
//                //            }
//                //        }
//                //    }
//                //}
//            }
//            EntityManager.DestroyEntity(e);
//        });
//    }
//}
