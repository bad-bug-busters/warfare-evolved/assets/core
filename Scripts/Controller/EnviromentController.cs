﻿using System;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

[ExecuteAlways]
public class EnviromentController : MonoBehaviour
{
    public SimTime time;

    [Range(1, 86400)] public float secondsPerDay = 86400;
    [Range(1, 365)] public float daysPerYear = 365;

    public bool simulate;
    [Range(0, 3600)] public float updateRate = 1;

    public bool flipCardinalDirections;

    public Planet planet = new Planet { latitude = 45, axialTilt = 23.5f };
    public Transform outerTerrain;
    public CelestialBody[] celestialBodies = new CelestialBody[2];

    public Quaternion spaceRotationOffset = Quaternion.Euler(0, 180, 0);

    public VolumeProfile volumeProfile;

    public Wind wind;

    [Range(-0.75f, 0f)] public float lightSwitch = -0.125f;
    [Range(-0.75f, 0f)] public float shadowSwitch = -0.0625f;
    [Range(0, 0.25f)] public float shadowBlend = 0.0625f;

    Quaternion fixedRot;
    Quaternion yearRot;
    Quaternion planetRot;

    HDRenderPipeline hdrp;
    HDAdditionalLightData[] lightData;
    VisualEnvironment env;
    PhysicallyBasedSky pbs;

    float updateTime;

    float dayTimeMod;
    float yearTimeMod;

    float2 windVector;

    void OnValidate()
    {
        Setup();
        UpdateSky();
    }

    void Start()
    {
        Setup();
    }

    void LateUpdate()
    {
        if (simulate)
        {
            time.day += dayTimeMod * Time.deltaTime;
            if (time.day >= 1)
                time.day -= 1;

            time.year += yearTimeMod * Time.deltaTime;
            if (time.year >= 1)
                time.year -= 1;

            if (Time.time >= updateTime)
            {
                updateTime += updateRate;
                UpdateSky();
            }
        }
        wind.maskOffset += windVector * Time.deltaTime;
        Shader.SetGlobalVector("_WindMaskOffset", (Vector2)wind.maskOffset);

        EnviromentData.time = time;
        EnviromentData.wind = wind;
    }

    void Setup()
    {
        updateTime = Time.time;
        dayTimeMod = 1f / secondsPerDay;
        yearTimeMod = 1f / (secondsPerDay * daysPerYear);

        fixedRot = Quaternion.Euler(90 - planet.latitude, flipCardinalDirections ? 0 : 180, 0);
        yearRot = Quaternion.Euler(0, time.year * 360, 0);

        planetRot = Quaternion.Euler(planet.axialTilt, 0, 0) * yearRot;

        hdrp = (HDRenderPipeline)RenderPipelineManager.currentPipeline;

        lightData = new HDAdditionalLightData[celestialBodies.Length];
        for (int i = 0; i < celestialBodies.Length; i++)
        {
            lightData[i] = celestialBodies[i].transform.GetComponent<HDAdditionalLightData>();
        }

        VisualEnvironment envTmp;
        if (volumeProfile.TryGet(out envTmp))
        {
            env = envTmp;
            env.windOrientation.value = wind.orientation;
            env.windSpeed.value = wind.speed;

            var orientationRad = math.radians(wind.orientation);
            var windDirection = new float2(math.cos(orientationRad), math.sin(orientationRad));
            windVector = windDirection * wind.speed / 3.6f;

            Shader.SetGlobalVector("_WindVector", new Vector3(windVector.x, 0, windVector.y));
            Shader.SetGlobalFloat("_WindTurbolence", wind.turbolence);
            Shader.SetGlobalTexture("_WindMaskTexture", wind.maskTexture);
            Shader.SetGlobalFloat("_WindMaskScale", wind.maskScale);
        }

        PhysicallyBasedSky pbsTmp;
        if (volumeProfile.TryGet(out pbsTmp))
        {
            pbs = pbsTmp;
            pbs.planetRotation.value = (Quaternion.Euler(-90 + planet.latitude, 0, 0) * Quaternion.Euler(0, planet.longitude, 0)).eulerAngles;
        }

        for (int i = 1; i < celestialBodies.Length; i++)
        {
            var interval = 360f / celestialBodies[i].cookies.Length;
            var cookie = (celestialBodies[i].rotationOffset.eulerAngles.y + interval / 2) / interval;
            if (cookie < 0)
                cookie += celestialBodies[i].cookies.Length;

            lightData[i].surfaceTexture = celestialBodies[i].cookies[(int)cookie];
        }


        var scale = 2 * math.PI * pbs.planetaryRadius.value / 360;
        var latScale = scale * math.cos(math.radians(planet.latitude));
        Debug.Log(latScale);
        outerTerrain.localScale = new Vector3(latScale, scale, scale);
        outerTerrain.position = new Vector3(planet.longitude * latScale, 0, planet.latitude * scale);
    }

    void UpdateSky()
    {
        var dayRot = Quaternion.Euler(0, time.day * 360, 0);
        var modRot = fixedRot * dayRot * Quaternion.Inverse(yearRot) * planetRot;
        var dotVal = new float[celestialBodies.Length];
        for (int i = 0; i < celestialBodies.Length; i++)
        {
            celestialBodies[i].transform.rotation = modRot * celestialBodies[i].rotationOffset;
            dotVal[i] = math.dot(celestialBodies[i].transform.rotation * Vector3.forward, Vector3.down);

            var col = 1 - math.clamp(dotVal[i], lightSwitch, 0) / lightSwitch;
            lightData[i].color = new Color(col, col, col);
            lightData[i].shadowDimmer = 1 - Mathf.Clamp01((i == 0 ? shadowSwitch + shadowBlend - dotVal[0] : dotVal[0] - shadowSwitch + shadowBlend) / shadowBlend);
            lightData[i].EnableShadows(dotVal[0] >= shadowSwitch ? (i == 0 ? true : false) : (i == 0 ? false : true));
        }
        var sunPos = 1 - math.acos(dotVal[0]) / math.PI;
        EnviromentData.sunPos = time.day < 0.5f ? -sunPos : sunPos;
        //Debug.Log("sun: " + EnviromentData.sunPos);

        pbs.spaceRotation.value = (fixedRot * dayRot * yearRot * spaceRotationOffset).eulerAngles;
        if (hdrp != null)
        {
            hdrp.RequestSkyEnvironmentUpdate();
        }
    }
}
